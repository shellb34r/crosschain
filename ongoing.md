This is the file that keeps track of the ongoing efforts

# current working items
Hakkyung and Ryan are setting up blockchain on Pi
Xinya is starting to work on monitoring/measuring data.

# things that need to be put on pi
- PoS: iotex, ethereum PoS?
- PoW: ethereum 
- Permissioned: hyperledger

# todos  
- Monitor and make measurements of the following on RPi:  
    -power  
    -cpu usage  
    -network  
    -block generalization
- Visualize the data we collect
- Control and Manipulate network packets
