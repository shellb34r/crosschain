### What we've tried/done so far
 
1. Ethereum (PoW | Raspbian32)
    - Failed due to RAM limitations
```
ERROR[06-20|16:12:28.149] Failed to generate mapped ethash dataset epoch=0 err="cannot allocate memory"
runtime: out of memory: cannot allocate 2147483648-byte block (128974848 in use)
fatal error: out of memory
```
Found this, which is our evidence of trying 64 bit OS later: 
https://ethereum.stackexchange.com/questions/50455/minimum-theoretical-hardware-requirement-for-geth

2. Ethereum (PoS | Raspbian32, gentoo64, Debian-buster64, Ubuntu64)
    - Raspbian32 failed due to dependency issue (couldn't install bazel)
    - Gentoo64 failed due to dependency issue - couldn't install basic packages (needed for bazel)
    - Buster failed due to RAM limitations
    - Ubuntu64 is work in progress
3. Hyperledger Fabric (PoS | Raspbian32)
    - Failed due to dependency issue? Had an extremely hard time finding docker images build on Armv7. Tried installing on Ubuntu virtual machine and it looked alright so maybe try with ubuntu on pi?
4. Iotex
    - got it installed but couldn't mine because the command line interface was unfinished
5. PROMISING: Ethereum (PoW | Ubuntu64)
    - Able to mine on one node, will expand to three nodes asap
6. Ethereum (PoA | Ubuntu64)
    - Currently looking into it
7. PROMISING: Qtum (PoS | Ubuntu64)
    - Currently setting it up
8. Stratis (PoS | Ubuntu)
    - Not able to run full node on RPi (only wallet)
9. Wei's PoS skeleton Simulation
    - Got it running on an adhoc network with three nodes and able to simulate PoS

