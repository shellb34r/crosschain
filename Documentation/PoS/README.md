# Qtum (PoS) on Raspberry Pi
Qtum is a [UTXO](https://en.wikipedia.org/wiki/Unspent_transaction_output) model-based blockchain with PoS consensus. After a trial of Ethereum Prysm on Raspberry Pi(RPi) turned out to be not successful, Qtum is considered as the next PoS candidate. While Qtum also supports Smart Contract feature by adopting Ethereum Virtual Machine (EVM), it is not covered as it is out of scope. This guide is based on the [official document](https://docs.qtum.site/en/), and **it is tested on on Ubuntu 18.04.02 LTS (arm64) on Raspberry Pi 3B+ model.**

### 1. Installing Qtum
Since Qtum officially supports ARM, we can easily get their official release from the repository. The following steps will get Qtum Core daemon and Qtum RPC client on RPi.
```
    wget https://github.com/qtumproject/qtum/releases/download/mainnet-ignition-v0.17.6/qtum-0.17.6-aarch64-linux-gnu.tar.gz
    tar xvzf qtum-0.17.6-aarch64-linux-gnu.tar.gz
    rm qtum-0.17.6-aarch64-linux-gnu.tar.gz
    sudo cp ./qtum-0.17.6/bin/qtumd /bin/
    sudo cp ./qtum-0.17.6/bin/qtum-cli /bin/
```
Once we finish the above steps, verify the status by running the followings:
`qtumd -version` will show something like
```
Qtum Core Daemon version mainnet-ignition-v0.17.6
Copyright (C) 2019 The Qtum Core developers

Please contribute if you find Qtum Core useful. Visit <https://qtum.org> for
further information about the software.
The source code is available from <https://github.com/qtumproject/qtum>.

This is experimental software.
Distributed under the MIT software license, see the accompanying file COPYING
or <https://opensource.org/licenses/MIT>

This product includes software developed by the OpenSSL Project for use in the
OpenSSL Toolkit <https://www.openssl.org> and cryptographic software written by
Eric Young and UPnP software written by Thomas Bernard.
```
and `qtum-cli -version` will show something like 
```
Qtum Core RPC client version mainnet-ignition-v0.17.6
```

### 2. Setting up a private network with PoS Consensus
While Ethereum allows a user to create a private network with the same functionality of mainnet, Qtum doesn't. To set up a private network, each node needs to be run in `regtest` mode, which comes with slightly restricted behaviors. Since we are using `regtest` mode, the whole process of creating a private network is a lot simpler than that of Ethereum.

#### # Configuring the first node
Qtum allows a configuration file called `qtum.conf`, which stays under `~/.qtum`. Putting command line options into this `qtum.conf` will keep steps more concise. Since this directory is automatically generated once Qtum node starts for the first time, we have to create this manually. Run the following commands: 
```
> cd
> mkdir .qtum
> touch qtum.conf
```
Now type the following options in `qtum.conf`:
```
regtest=1
listen=1
rpcuser=test
rpcpassword=test

[regtest]
port=8330
rpcport=13889
```
We can now run a Qtum node just by running `qtumd` command. Qtum will automatically look for `~/.qtum/qtum.conf` at the time of launch and use it if exists. We can also run the node in the background by passing `-daemon`, or even add a line `daemon=1` to `qtum.conf`. Also, if we don't specify the port under `regtest`, the default port for `regtest` is 23888. 

Within the `regtest` mode, The first 501 blocks should be manually generated with a command, and these manually created blocks are considered as `PoW` blocks. As soon as the block number hits 501, a node starts staking and creating a block every 30 seconds. This block generation rate is fixed strictly within the `regtest` mode, which is one different behavior. To see this, we first create 501 blocks manually. Before we do this, let's quickly test RPC request. 

Qtum uses RPC for communication, and a user can use either `qtum-cli` or manually send a request via `curl`. Assuming that `qtumd` is running, run the following to fetch the information of blockchain. If `qtumd` is not running in the background, make sure to open a new terminal and ssh into the same machine. 
##### # qtum-cli method
```
> qtum-cli getblockchaininfo

{
  "chain": "regtest",
  "blocks": 0,
  "headers": 0,
  "bestblockhash": "665ed5b402ac0b44efc37d8926332994363e8a7278b7ee9a58fb972efadae943",
  "difficulty": 4.656542373906925e-10,
  "moneysupply": 0,
  "mediantime": 1504695029,
  "verificationprogress": 1,
  "initialblockdownload": true,
  "chainwork": "0000000000000000000000000000000000000000000000000000000000000002",
  "size_on_disk": 383,
  "pruned": false,
  "softforks": [
    {
      "id": "bip34",
      "version": 2,
      "reject": {
        "status": true
      }
    },
    {
      "id": "bip66",
      "version": 3,
      "reject": {
        "status": true
      }
    },
    {
      "id": "bip65",
      "version": 4,
      "reject": {
        "status": true
      }
    }
  ],
  "bip9_softforks": {
    "csv": {
      "status": "defined",
      "startTime": 0,
      "timeout": 999999999999,
      "since": 0
    },
    "segwit": {
      "status": "defined",
      "startTime": 0,
      "timeout": 999999999999,
      "since": 0
    }
  },
  "warnings": ""
}
```
##### # Direct curl method
```
> curl --user test:test --data-binary '{"jsonrpc": "1.0", "id":"1", "method": "getblockchaininfo", "params": [] }' -H 'content-type: text/plain;' http://127.0.0.1:13889/

{"result":{"chain":"regtest","blocks":0,"headers":0,"bestblockhash":"665ed5b402ac0b44efc37d8926332994363e8a7278b7ee9a58fb972efadae943","difficulty":4.656542373906925e-10,"moneysupply":0,"mediantime":1504695029,"verificationprogress":1,"initialblockdownload":true,"chainwork":"0000000000000000000000000000000000000000000000000000000000000002","size_on_disk":383,"pruned":false,"softforks":[{"id":"bip34","version":2,"reject":{"status":true}},{"id":"bip66","version":3,"reject":{"status":true}},{"id":"bip65","version":4,"reject":{"status":true}}],"bip9_softforks":{"csv":{"status":"defined","startTime":0,"timeout":999999999999,"since":0},"segwit":{"status":"defined","startTime":0,"timeout":999999999999,"since":0}},"warnings":""},"error":null,"id":"1"}
```
Notice that we get the same results. Since we haven't created any blocks, there are 0 `blocks` and the `moneysupply` is also 0. From now on, we will be using `qtum-cli` for simplicity and readability. We will now check the wallet information.
```
> qtum-cli getwalletinfo

{
  "walletname": "",
  "walletversion": 169900,
  "balance": 0.00000000,
  "stake": 0.00000000,
  "unconfirmed_balance": 0.00000000,
  "immature_balance": 0.00000000,
  "txcount": 0,
  "keypoololdest": 1564601170,
  "keypoolsize": 1000,
  "keypoolsize_hd_internal": 1000,
  "paytxfee": 0.00000000,
  "hdseedid": "9471035cb09f40384216e4d747a7bff8460441c6",
  "hdmasterkeyid": "9471035cb09f40384216e4d747a7bff8460441c6",
  "private_keys_enabled": true
}
```
We can see the expected values of 0. We can also take a look at staking information.
```
> qtum-cli getstakinginfo

{
  "enabled": true,
  "staking": false,
  "errors": "",
  "currentblocktx": 0,
  "pooledtx": 0,
  "difficulty": 4.656542373906925e-10,
  "search-interval": 0,
  "weight": 0,
  "netstakeweight": 0,
  "expectedtime": 0
}
```
We can see that currently we are not staking anything. Now we can run the following command to create 501 blocks manually:
```
> qtum-cli generate 501

[
"47d8b91f51a2046ae5df3115e89e2c7361e33b0e51d9859274e5eb2e318da761",
"04c7e13003690b06f4a9edc44f77d461e2b695ede324cce3e36c491750caddf0",
...
"5b79be607625e62ba156ef5926629d51d9ed43c8e8ca92e05cfb45baea0d0557",
"729bb3973a4064ba5cec2aef3a8f24ad382c0fb0b685040796442d18f575c605"
]
```
The returned list contains 501 block hashes we just created. To verify that manually created block is `PoW` block, let's take a look at the first block 1 using its block hash.
```
> qtum-cli getblock "47d8b91f51a2046ae5df3115e89e2c7361e33b0e51d9859274e5eb2e318da761"

{
  "hash": "47d8b91f51a2046ae5df3115e89e2c7361e33b0e51d9859274e5eb2e318da761",
  "confirmations": 502,
  "strippedsize": 327,
  "size": 327,
  "weight": 1308,
  "height": 1,
  "version": 536870912,
  "versionHex": "20000000",
  "merkleroot": "b2244a19c03baf73073e0d5a1361a05bf4655c366f620bf971876b873d8dda1e",
  "hashStateRoot": "9514771014c9ae803d8cea2731b2063e83de44802b40dce2d06acd02d0ff65e9",
  "hashUTXORoot": "21b463e3b52f6201c0ad6c991be0485b6ef8c092e64583ffa655cc1b171fe856",
  "tx": [
    "b2244a19c03baf73073e0d5a1361a05bf4655c366f620bf971876b873d8dda1e"
  ],
  "time": 1564601471,
  "mediantime": 1564601471,
  "nonce": 1,
  "bits": "207fffff",
  "difficulty": 4.656542373906925e-10,
  "chainwork": "0000000000000000000000000000000000000000000000000000000000000004",
  "nTx": 1,
  "previousblockhash": "665ed5b402ac0b44efc37d8926332994363e8a7278b7ee9a58fb972efadae943",
  "nextblockhash": "04c7e13003690b06f4a9edc44f77d461e2b695ede324cce3e36c491750caddf0",
  "flags": "proof-of-work",
  "proofhash": "47d8b91f51a2046ae5df3115e89e2c7361e33b0e51d9859274e5eb2e318da761",
  "modifier": "27846beb8fce524c6bac12e19d71df75a9ca84435b87d99e1f5065c3171674f0"
}
```
The `flags` tells us that the block is in fact `PoW` block. As mentioned before, after we generate 501 blocks, we start staking and generating PoS blocks. To verify this, we can check the blockchain information, wallet information and staking information as we did previously.
```
> qtum-cli getblockchaininfo

{
  "chain": "regtest",
  "blocks": 513,
  "headers": 513,
  "bestblockhash": "091a9dddcfc0411264ee9d7566871fc061708e0678b150dec46e6f310648a1fe",
  "difficulty": 4.656542373906925e-10,
  "moneysupply": 10260000,
  "mediantime": 1564601760,
  "verificationprogress": 1,
  "initialblockdownload": false,
  "chainwork": "0000000000000000000000000000000000000000000000000000000000000404",
  "size_on_disk": 200622,
  "pruned": false,
  "softforks": [
    {
      "id": "bip34",
      "version": 2,
      "reject": {
        "status": true
      }
    },
    {
      "id": "bip66",
      "version": 3,
      "reject": {
        "status": true
      }
    },
    {
      "id": "bip65",
      "version": 4,
      "reject": {
        "status": true
      }
    }
  ],
  "bip9_softforks": {
    "csv": {
      "status": "active",
      "startTime": 0,
      "timeout": 999999999999,
      "since": 432
    },
    "segwit": {
      "status": "active",
      "startTime": 0,
      "timeout": 999999999999,
      "since": 432
    }
  },
  "warnings": ""
}
```
Notice that now the `blocks` is 513, which is more than what we manually generated. Also note that the `moneysupply` is now 10260000, which comes from 513 * 20000. 
```
> qtum-cli getwalletinfo

{
  "walletname": "",
  "walletversion": 169900,
  "balance": 20000.00000000,
  "stake": 480000.00000000,
  "unconfirmed_balance": 0.00000000,
  "immature_balance": 9760000.00000000,
  "txcount": 513,
  "keypoololdest": 1564601170,
  "keypoolsize": 999,
  "keypoolsize_hd_internal": 1000,
  "paytxfee": 0.00000000,
  "hdseedid": "9471035cb09f40384216e4d747a7bff8460441c6",
  "hdmasterkeyid": "9471035cb09f40384216e4d747a7bff8460441c6",
  "private_keys_enabled": true
}
```
Unlike before, we see some changes. As a note, the reward for generating a block in the `regtest` mode is 20000, and each earned reward requires 500 blocks of maturity to be used for staking or balance. Here, we see that the `balance` is 20000, `stake` is 480000, and `immature_balance` is 9760000.00000000. What balance represents is the actual balance a user can use. It comes from the reward of the 1st block. Since staking starts from 501 block, the whole reward of 20000 goes into `balance`. `stake` is the amount a user is staking. `immature_balance` is the balance that requires some maturity to be able to use. We can see that for every 30 seconds, a new block is generated, and the `stake` increases by 40000 and `immature_balance` decreases by 20000. What actually happens is that since the node starts staking from 501 block, the new reward of 20000 earned from generating a new `PoS` block goes into `stake` automatically, and another block reward of 20000 that we earned from generating `PoW` blocks but was held as `immature_balance` now satisfies the 500 blocks of maturity and thus goes into `stake`. If we look at the block hash of 502 block, we see that it is actually `PoS` block. We first get the block hash.
```
> qtum-cli getblockhash 502
20bbb0456309629b96d712078693dfaba3eae88935fb809ad769180d9b3f92ac
```
Then we can take a look at the block using the block hash. 
```
> qtum-cli getblock "20bbb0456309629b96d712078693dfaba3eae88935fb809ad769180d9b3f92ac"

{
  "hash": "20bbb0456309629b96d712078693dfaba3eae88935fb809ad769180d9b3f92ac",
  "confirmations": 96,
  "strippedsize": 583,
  "size": 619,
  "weight": 2368,
  "height": 502,
  "version": 536870912,
  "versionHex": "20000000",
  "merkleroot": "2e06f6051ef16494819b15c3848727a6982e23352ed89ec900572ab5e6e4eebd",
  "hashStateRoot": "9514771014c9ae803d8cea2731b2063e83de44802b40dce2d06acd02d0ff65e9",
  "hashUTXORoot": "21b463e3b52f6201c0ad6c991be0485b6ef8c092e64583ffa655cc1b171fe856",
  "tx": [
    "d4b5f6e472f7910cfc178093f7d4c9c8ddd2d8e119a7826b52d4442896722678",
    "b7b94d1ea8b974409db4a006082e2f6cc52089bdd3a7424e60136eae7197cd3a"
  ],
  "time": 1564601568,
  "mediantime": 1564601555,
  "nonce": 0,
  "bits": "207fffff",
  "difficulty": 4.656542373906925e-10,
  "chainwork": "00000000000000000000000000000000000000000000000000000000000003ee",
  "nTx": 2,
  "previousblockhash": "729bb3973a4064ba5cec2aef3a8f24ad382c0fb0b685040796442d18f575c605",
  "nextblockhash": "a716cefb4afa3060b1a4fb313bf7bc604a8b0b78c636031a4914d9e029430801",
  "flags": "proof-of-stake",
  "proofhash": "6180e37df3fa01d07a339cd01d9a341d7bfdf60f0d2b0f93374838b8432608c9",
  "modifier": "672bfb4fe63461b27a891e130033c28155a33193bab35f18270d708bf9f708f8",
  "signature": "3044022026a5fcc80e6ad08ca8f1e7601a0ffd9669f91267ecac41adcf0cf6f3fb76bada02207f3b71c8749f05a8dfe8d7b62ed74b75f64c421819e4926600791ea3dbb80a1d"
}
```
As expected, `flags` shows that the block is `PoS` block. 

#### # Connecting peers
Now that the chain is running, we can connect peers to the network. Just like we did previously, create `qtum.conf` file. Then run the second node with an additional command line argument: `qtumd -connect=<ip address>:<port>`. For this example, it would look like:
```
> qtumd -connect=pi_node7:8330

...
2019-07-31T21:10:00Z UpdateTip: new best=1d187766218e106b6df07aa062d59bf1a07ff9d968aeef7cb72b843513607158 height=673 version=0x20000000 log2_work=10.396605 tx=846 date='2019-07-31T21:08:48Z' progress=1.000000 cache=0.2MiB(1189txo)
2019-07-31T21:10:00Z ProcessNetBlock: ACCEPTED
...
```
Obviously, this can be done by adding lines to the `qtum.conf` as following:
```
[regtest]
connect=<ip address>:<port>
...
```
Notice that we can specify multiple connections. This way, we can simply run `qtumd`, and the connection will be established. As soon as it connects to the other node, the node starts fetching blocks and we can see the progress. The connection can be verified from the original node, which in this case is `pi_node7`. Run the following command to check the number of connected nodes:
```
> qtum-cli getconnectioncount
1
```
The count doesn't include itself, and we know that there's another node connected to itself. 

# Note for tomorrow: need at least two staking nodes for testing transactions

Reference: [link1](https://bitcoin.stackexchange.com/questions/28107/bitcoin-is-not-connected-in-regtest-mod) [link2](https://samsclass.info/141/proj/pBitc2.htm)

### Transactions functionality
- difficulty  
command: ```qtum-cli -regtest getmininginfo```  
json::difficulty::proof-of-stake  
json::difficulty::proof-of-stake  
- the block size  
first we need to find the hash of a block  
```qtum-cli -regtest getblockhash height(int)```  
then we use the hash to get the info of this particular block  
``` qtum-cli -regtest getblock hash(str)```  
json::size  
- time it needs to produce a block  
Others told me the block is generated in a regular frequency. I will double check that, but there is no good command to get that for now. Unless you want to check the two timstamp and do the math.  
- number of transactions  
``` qtum-cli -regtest getblock hash(str)```  
json::nTx  

### Transition into Proof-of-stake
According to [this post](https://steemit.com/qtum/@cryptominder/qtum-blockchain-development-environment-setup), for the first of 5000 blocks, Qtum is in a mixed state of PoW and PoS. For testing purpose, please use ```qtum-cli -regtest generate $number_of_blocks``` to reach 5000 quickly, and try to keep the generation even, as it affects balance too. For the first time set up, wait a few hours to let the state transits into PoS. To check it, select the latest block, and use ```qtum-cli -regtest getblock $hash_of_the_block```. It should be "proof-of-stake" in the flag. Also, avoid generating blocks simultaneously on multiple nodes to prevent wierd syncing issues.   

### To put everything inside the crosschain folder
I come up with a hacky way to do it... as I do not want to change the source code of Qtum and recompile it. As qtum will put everything in ```~/.qtum``` directory, we can move the directory to another place, and create a softlink for qtum to use. 
If you have not run qtum yet, 
```
mkdir $dir_you_want
```
or if you have qtum run already (check if you have ```~/.qtum``` exist or not), 
```
mv ~/.qtum $dir_you_want
```
And then: 
```
ln -s  $dir_you_want ~/.qtum
```
to creat a soft link at the path qtum uses. 