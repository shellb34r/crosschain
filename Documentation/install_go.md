# Install GO
1. Download the latest GO version from the following website: [golang.org/dl/]. Be sure to get the ARM version.
2. `sudo tar -C /usr/local -xzf $file`  
3. `vim ~/.profile`  
4. Add the following to the bottom of the file: "export PATH=$PATH:/usr/local/go/bin"  
5. `source ~/.profile`