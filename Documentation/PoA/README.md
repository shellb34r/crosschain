# Ethereum Parity (PoA) on Raspberry Pi

Parity, along with Geth, is one of the two most popular Ethereum clients. While both support Proof-of-Authority (PoA) consensus engines, Aura on Parity and Clique on Geth, here we are going to introduce how to set up a private network with Parity on multiple Raspberry Pis. This guide is based on the official [Parity Wiki](https://wiki.parity.io/Parity-Ethereum).

**The following guide is tested on Ubuntu 18.04.02 LTS (arm64) on Raspberry Pi 3B+ model.**


### 1. Installing Parity 
While the official [Parity manual](https://github.com/paritytech/parity-ethereum) provides a way of installing Parity from the source, we observed an *out-of-memory* error from one of its main dependencies, rustc, a Rust programming language compiler. The official Parity client is available via Snap, an Ubuntu package manager. Simply issuing the following command on the terminal will install Parity.  
`$ sudo snap install parity`  
Installing Parity via Snap is not only just simple but also helpful for keeping it up to date in a sense that Snap automatically updates its package when a new version is available.
Once it is installed, we can run `$ parity --version` to see if it is installed correctly. We should get something like the following:
```
Parity Ethereum
  version Parity-Ethereum/v2.5.5-stable-3ebc769-20190708/aarch64-linux-gnu/rustc1.34.2
Copyright 2015-2019 Parity Technologies (UK) Ltd.
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

By Wood/Paronyan/Kotewicz/Drwięga/Volf
   Habermeier/Czaban/Greeff/Gotchac/Redmann
```

### 2. Setting up a private network with PoA consensus
Just like setting up a private network using Geth, we need to have our own genesis file ready that defines the genesis block of our blockchain. However, unlike Proof-of-Work (PoW) or Proof-of-Stake (PoS) based blockchain, we need to specify validators at the beginning of initialization. Once a validator initializes the chain, other nodes can join the network. In this guide, we are going to demonstrate a case with one validator node and one non-validator node, each on different machines: node0 and node1, respectively. First, we are going to create a validator node.

##### # Configuring a validator node
Create a new directory, call it `poa` here, and create a `poa_demo.json` using the following snippet. 
```
{
    "name": "poa_demo",
    "engine": {
        "authorityRound": {
            "params": {
                "stepDuration": "5",
                "validators" : {
                    "multi": {
                        "0": {
                            "list": [""]
                        }
                    }
                }
            }
        }
    },
    "params": {
        "gasLimitBoundDivisor": "0x400",
        "maximumExtraDataSize": "0x20",
        "minGasLimit": "0x7A1200",
        "networkID" : "0x2323",
        "eip155Transition": 0,
        "validateChainIdTransition": 0,
        "eip140Transition": 0,
        "eip211Transition": 0,
        "eip214Transition": 0,
        "eip658Transition": 0,
        "eip150Transition": "0x0",
        "eip160Transition": "0x0",
        "eip161abcTransition": "0x0",
        "eip161dTransition": "0x0",
        "eip98Transition": "0x7fffffffffffff",
        "maxCodeSize": 24576,
        "maxCodeSizeTransition": "0x0"
    },
    "genesis": {
        "seal": {
            "authorityRound": {
                "step": "0x0",
                "signature": "0x0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
            }
        },
        "difficulty": "0x20000",
        "gasLimit": "0x7A1200"
    },
    "accounts": {
        "0x0000000000000000000000000000000000000001": { "balance": "1", "builtin": { "name": "ecrecover", "pricing": { "linear": { "base": 3000, "word": 0 } } } },
        "0x0000000000000000000000000000000000000002": { "balance": "1", "builtin": { "name": "sha256", "pricing": { "linear": { "base": 60, "word": 12 } } } },
        "0x0000000000000000000000000000000000000003": { "balance": "1", "builtin": { "name": "ripemd160", "pricing": { "linear": { "base": 600, "word": 120 } } } },
        "0x0000000000000000000000000000000000000004": { "balance": "1", "builtin": { "name": "identity", "pricing": { "linear": { "base": 15, "word": 3 } } } },
        "0x0000000000000000000000000000000000000005": { "builtin": { "name": "modexp", "activate_at": 0, "pricing": { "modexp": { "divisor": 20 } } } },
        "0x0000000000000000000000000000000000000006": { "builtin": { "name": "alt_bn128_add", "activate_at": 0, "pricing": { "linear": { "base": 500, "word": 0 } } } },
        "0x0000000000000000000000000000000000000007": { "builtin": { "name": "alt_bn128_mul", "activate_at": 0, "pricing": { "linear": { "base": 40000, "word": 0 } } } },
        "0x0000000000000000000000000000000000000008": { "builtin": { "name": "alt_bn128_pairing", "activate_at": 0, "pricing": { "alt_bn128_pairing": { "base": 100000, "pair": 80000 } } } }
    }
}
```
While it is possible to run launch Parity with command line options, for the sake of simplicity, we are going to maintain a config file named `node0.toml`. Use the following snippet and save it under the `/poa`.
```
# File node0.toml

[parity]
chain = "poa_demo.json"
base_path = "node0"
[network]
port = 30300
[rpc]
port = 8540
apis = ["web3", "eth", "net", "personal", "parity", "parity_set", "traces", "rpc", "parity_accounts"]
[websockets]
port = 8450
```
Note that instead of having actual port numbers and other options, we could have `disable = true` for `[rpc]` and `[websockets]` sections. We are going to keep it as it is for now. Also, if we want to enable RPC call from outside, we should add `interface = "all"` under `[rpc]` section. Having `node0.toml` file ready, let's create a new account for node0. Within `/poa`, run the following command:
`$ parity --config node0.toml account new`.  
Then it will ask for a password. For simplicity, we are going to use `node0pwd` as the password for node0. When we successfully create a new account, we see a new address on the console, which is the address of node0. Keep notes of this address. Assume that the address we get is `0xa2922fec00bb29fe13d68e87f64b9ad1719ed64a`. Also, once an account is created, running `$ echo "node0pwd" > node0.pwd` will keep the password in `node0.pwd` file. Now open `node0.toml` file again and add the following snippet:
```
[account]
password = ["node0.pwd"]

[mining]
engine_signer = "0xa2922fec00bb29fe13d68e87f64b9ad1719ed64a"
reseal_on_txs = "none"
force_sealing = true
```
##### # Configuring a non-validator node
Configuring a non-validator node is pretty much the same as what we did for a validator node. We just need to a new account for node1 and a different config file. First, copy `poa_demo.json` file that we used for initializing the chain to node1's machine. Then create `node1.toml`, which looks like the following:
```
# File node1.toml

[parity]
chain = "poa_demo.json"
base_path = "node1"
[network]
port = 30303
[rpc]
port = 8540
apis = ["web3", "eth", "net", "personal", "parity", "parity_set", "traces", "rpc", "parity_accounts"]
[websockets]
port = 8450
interface = "local"
```
Just like before, run `$ parity --config node1.toml account new` to generate a new account for node1. Here, we use `node1pwd` as password, and run `$ echo "node1pwd" > node1.pwd` to keep it in a separate file. Assuming that the generated address is `0x12db1ee91481573302f63ebf3d735820081c68a2`, open `node1.toml` and add the following snippet:
```
[account]
unlock = ["0x12db1ee91481573302f63ebf3d735820081c68a2"]
password = ["node1.pwd"]
```

##### # Initializing the chain
Now open `poa_demo.json` again and add node0's address in the value field of `"list"` on every node. It should look like this:
`"list": ["0xa2922fec00bb29fe13d68e87f64b9ad1719ed64a"]`.
If we want more than one validator, we can repeat the previous section and add multiple addresses, separating them with a comma.  
`ex) "list": ["0xa2922fec00bb29fe13d68e87f64b9ad1719ed64a", "<another address>"]`  

**One thing to note is that once a chain is initialized, we can add additional validators only through a hard fork.**

Also, to give node1 (or to any other desired nodes) some initial balance, add the following line under `"accounts"`:
`"<address of a node>": { "balance": "desired amount" }`. For our case, the `"accounts"` section should look something like the following:
```
"accounts": {
        "0x0000000000000000000000000000000000000001": { "balance": "1", "builtin": { "name": "ecrecover", "pricing": { "linear": { "base": 3000, "word": 0 } } } },
        "0x0000000000000000000000000000000000000002": { "balance": "1", "builtin": { "name": "sha256", "pricing": { "linear": { "base": 60, "word": 12 } } } },
        "0x0000000000000000000000000000000000000003": { "balance": "1", "builtin": { "name": "ripemd160", "pricing": { "linear": { "base": 600, "word": 120 } } } },
        "0x0000000000000000000000000000000000000004": { "balance": "1", "builtin": { "name": "identity", "pricing": { "linear": { "base": 15, "word": 3 } } } },
        "0x0000000000000000000000000000000000000005": { "builtin": { "name": "modexp", "activate_at": 0, "pricing": { "modexp": { "divisor": 20 } } } },
        "0x0000000000000000000000000000000000000006": { "builtin": { "name": "alt_bn128_add", "activate_at": 0, "pricing": { "linear": { "base": 500, "word": 0 } } } },
        "0x0000000000000000000000000000000000000007": { "builtin": { "name": "alt_bn128_mul", "activate_at": 0, "pricing": { "linear": { "base": 40000, "word": 0 } } } },
        "0x0000000000000000000000000000000000000008": { "builtin": { "name": "alt_bn128_pairing", "activate_at": 0, "pricing": { "alt_bn128_pairing": { "base": 100000, "pair": 80000 } } } },
        "0x12db1ee91481573302f63ebf3d735820081c68a2": { "balance": "10000000000000000000000" }
    }
```
Now run `$ parity --config node0.toml` on node0 and verify that it runs without any error. During the launch, we can see node0's public node URL, which looks something like the following:
```
2019-07-09 20:17:18 UTC Public node URL: enode://37961412697a57bf73b39d7e785fbf9e82cf4820793dd61899dded6ce3e52f723f51f96f423f081a2549574b7045fff4439d6ee8278b0e1a16a7971a963c5ba3@172.16.9.178:30300
```
Keep this value as we'll need it later. Also, since node0 is a validator node, we should be able to see it sealing a block every 5 seconds, and it should look like something like the following:
```
2019-07-09 20:17:40 UTC Imported #1 0xb8de…99ba (0 txs, 0.00 Mgas, 1 ms, 0.57 KiB)
```

Now similarly run `$ parity --config node1.toml` on node1 and verify that it also runs without any issues. Again, we will see node1's public node URL, which we assume it to be the following:
```
2019-07-09 15:36:38  Public node URL: enode://e57dbdad225046709c00b73273bdcfb9b5536173b30106b10635ecd3c70ecf452077934af59ce6eb4677b327be891ed3d3a47e5f9715babdae33d84c88b64de2@172.16.9.108:30303
```
Again, we'll be using this in the next section. Note that for both nodes, we will see something like the following:
```
2019-07-09 15:37:04     0/25 peers   80 KiB chain 782 KiB db 0 bytes queue 448 bytes sync  RPC:  0 conn,    0 req/s,    0 µs
```
It tells us that there are no other nodes connected to myself. In the next section, we will see it changing when nodes are connected. 
At this point, we are ready to connect nodes to each other. Finish all running nodes by simply sending a signal via `Ctrl + C`.

##### # Connecting nodes with each other
Open `node0.toml` and `node1.toml`. For both files, add `bootnodes = ["<node0's public node URL>", "<node1's public node URL>"]` under `[network]` section and substitute the corresponding values. For our case, the network section should look something like the following:
```
[network]
port = 30303
bootnodes = ["enode://37961412697a57bf73b39d7e785fbf9e82cf4820793dd61899dded6ce3e52f723f51f96f423f081a2549574b7045fff4439d6ee8278b0e1a16a7971a963c5ba3@172.16.9.178:30300", "enode://e57dbdad225046709c00b73273bdcfb9b5536173b30106b10635ecd3c70ecf452077934af59ce6eb4677b327be891ed3d3a47e5f9715babdae33d84c88b64de2@172.16.9.108:30303"]
```
From now on, when Parity starts, each node will find each other automatically and sync. To verify this, run node0 and node1 and on each node, we should see something like the following:
```
2019-07-09 21:03:00 UTC    1/25 peers   87 KiB chain 793 KiB db 0 bytes queue 15 KiB sync  RPC:  0 conn,    0 req/s,    0 µs
```
Notice that the number increased from 0 to 1, which shows us that there is an additional node in the network excluding myself. At this point, our private blockchain with PoA is running!

### Working with CrossChain
To work with CrossChain, the chain folder must be placed in the following directory: `~/CrossChain/PoA/`. In the folder, a script `run.sh` must also be created with the following: `parity --config $FULLPATH_CONFIG.toml` where $FULLPATH_CONFIG is the absolute path of the config.toml file that should have been created. `run.sh` may need to have its permissioned changed to executable. This needs to be done for all nodes. 

### 3. Sending a transaction
Now that our chain is running with two nodes, we are going to demonstrate how to send a transaction from one node to another. We can communicate from a running node in two ways. We can either use JSON-RPC or web3.js. For a detailed list of available  JSON-RPC API, refer to https://wiki.parity.io/JSONRPC and for web3.js API, refer to https://web3js.readthedocs.io/en/1.0/index.html
##### # Using JSON-RPC call
Let's continue with our previous example. We can either open up a new terminal on the machine where a node is running or run Parity in the background from the beginning and work on the same terminal. For now, we are just going to use a new terminal here. To recap,
- node0's address: 0xa2922fec00bb29fe13d68e87f64b9ad1719ed64a
- node1's address: 0x12db1ee91481573302f63ebf3d735820081c68a2

Before we send balance, let's first check that the balance of node0 is actually 0 by running the following:
```
$ curl --data '{"jsonrpc":"2.0","method":"eth_getBalance","params":["0xa2922fec00bb29fe13d68e87f64b9ad1719ed64a", "latest"],"id":1}' -H "Content-Type: application/json" -X POST localhost:8545
```
When successful, we should see the following return value:
```
{"jsonrpc":"2.0","result":"0x0","id":1}
```
Now, let's send some balance from node1 to node0. For now, we are sending some arbitrary amount, 0x5. Run the following on **node1**'s terminal:
```
$ curl --data '{"jsonrpc":"2.0","method":"personal_sendTransaction","params":[{"from":"0x12db1ee91481573302f63ebf3d735820081c68a2","to":"0xa2922fec00bb29fe13d68e87f64b9ad1719ed64a","value":"0x5"}, "node1pwd"],"id":0}' -H "Content-Type: application/json" -X POST localhost:8545
```
When successful, we should see a transaction hash as a return value like the following:
```
{"jsonrpc":"2.0","result":"0xeb7da08e5cc7b1d4e08ac0061f44a741467d23e8cf4fb154fc120c770d83d0ba","id":0}
```
Also, we should be able to see a line on both nodes saying that the transaction we just made is included in the block:
```
2019-07-09 16:35:40  Imported #123 0x887f…b579 (1 txs, 0.03 Mgas, 5 ms, 0.74 KiB)
```
To verify that the balance is actually transferred from node1 to node0, run the `eth_getBalance` call again as we did above, and this time it should show like the following:
```
{"jsonrpc":"2.0","result":"0x5","id":1}
```

##### # Using web3.js
In order to use web3.js, we need to install it via npm. Run the following sequence:
```
$ sudo apt update
$ sudo apt install nodejs
$ sudo apt install npm
$ npm install web3
```
Assuming that web3 is installed correctly, launch Node console on the same machine within a new terminal where Parity node is running. For simplicity, we are going to demonstrate how to check balance. Within the Node console do the following steps:
```
> const Web3 = require('web3');
undefined
> const web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'), null, {});
undefined
> web3.eth.getBalance("0x12db1ee91481573302f63ebf3d735820081c68a2").then(console.log);
Promise {
  _bitField: 0,
  _fulfillmentHandler0: undefined,
  _rejectionHandler0: undefined,
  _promise0: undefined,
  _receiver0: undefined }
> 5
```
Depending on the situation or preference, we can also use WebsocketProvider. Notice that since web3.js returns a promise, we need to resolve it. As expected, the `getBalance` returned 5, which is identical to what we got with JSON RPC call.

### 4. Adding additional validators (Optional)
This chapter will demonstrate how to add a new validator by deploying a Smart Contract on our private network. As mentioned before, in order to add a new validator to the existing validator list, there must be a hard fork first. Then with the help of provided contract functions, we can easily add a new validator. When it comes to deploying a contract, the official document recommends us to use [Remix](https://remix.ethereum.org/).  
While it is undeniably true that Remix is intuitive and easy to use, the fact that Remix interacts with its daemon through a web browser makes our RPi impossible to use it concurrently with a running Parity node. As an alternative, we are going to use [Truffle](https://www.trufflesuite.com/) to deploy contracts. Keep in mind that since deploying contracts require some amount of gas, we need to set up the environment on the node with some initial balance. Here, we'll deploy contracts on the node1 machine.

##### # Setting up Truffle
- While we try not to use a swap memory on RPi throughout the guide, it turns out that it is inevitable to set up a swap space on the RPi that deploys contracts. Here's a quick guide on how to set up a swap file:
```
$ sudo fallocate -l 1G /swapfile
$ sudo chmod 600 /swapfile
$ sudo mkswap /swapfile
$ sudo swapon /swapfile
$ sudo vim /etc/fstab
    Add this line: /swapfile swap swap defaults 0 0
```
To check whether it is set up correctly, run `$ sudo swapon --show`, and we should have something like the following:
```
NAME      TYPE  SIZE   USED PRIO
/swapfile file 1024M 184.5M   -2
```
Truffle can be installed via npm. Run the following: `$ npm install -g truffle`. Once it is finished, clone [this repo](https://github.com/parity-contracts/kovan-validator-set), which is provided by Parity. Understanding the concept of contract is out of scope, so we skip any explanation regarding contracts or provided files.

What Truffle does is that it helps us compile contracts that are written in Solidity and deploy them to a target blockchain. In order to connect Truffle with our local chain, we need to modify `truffle-config.js`. When we open the file, it should look like this:
```
"use strict";

module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "*",
    },
  },
};
```
Change `port` value to `8545` to match our setting, and add the following snippet after `networks`:
```
 compilers: {
    solc: {
      version: "0.4.22",
    }
  }
  ```
While the provided `.sol` files require a compiler to be `"^0.4.22"`, we had a parsing issue with a newer version, and we were able to resolve once we match the exact compiler version. The complete `truffle-config.js` should look like this:
```
"use strict";

module.exports = {
  networks: {
    development: {
      host: "127.0.0.1",
      port: 8545,
      network_id: "*",
    },
  },
  compilers: {
    solc: {
      version: "0.4.22",
    }
  }
};
```
We can simply run `$ truffle compile`, and this will compile files in `/contracts`. When successful, we'll encounter some compilation warnings, but we can still proceed. 

##### # Deploying contracts

At this point, we should have our nodes running. We are going to deploy contracts in two steps; We will deploy `RelaySet` contract first, and then we will deploy `RelayedOwnedSet`. Create a file called `2_relay_deploy.js` using the following snippet under `/migrations`:
```
var Relay = artifacts.require("./RelaySet.sol");
module.exports = function(deployer){
        deployer.deploy(Relay);
};
```
We can simply run `$ truffle migrate`, and as long as we don't see any other issues, we should see something like the following as a result:
```
Compiling your contracts...
===========================
> Everything is up to date, there is nothing to compile.

Starting migrations...
======================
> Network name:    'development'
> Network id:      8995
> Block gas limit: 0x7a1200

1_initial_migration.js
======================

   Deploying 'Migrations'
   ----------------------
   > transaction hash:    0xea3cb450377568e60fd588875a3a9f8eb0311c2634e07fd51e2881de0174ddd5
   > Blocks: 0            Seconds: 0
   > contract address:    0x652092C11FAC09c206d637c7c49CcE37907Ccf44
   > block number:        6265
   > block timestamp:     1562785310
   > account:             0x12db1ee91481573302f63ebf3d735820081c68a2
   > balance:             9999.901709999999999985
   > gas used:            277654
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.00555308 ETH

   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.00555308 ETH

2_relay_deploy.js
=================

   Deploying 'RelaySet'
   --------------------
   > transaction hash:    0x6fd5d7ea127d9d145881a5b95bfe0d513f8ee954eaa9bcca0a0cdb78ab98c47f
   > Blocks: 1            Seconds: 4
   > contract address:    0x6f65d8841ef120D8B58b3494aB0f5f592CB9d246
   > block number:        6267
   > block timestamp:     1562785325
   > account:             0x12db1ee91481573302f63ebf3d735820081c68a2
   > balance:             9999.883077099999999985
   > gas used:            889637
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.01779274 ETH

   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.01779274 ETH

Summary
=======
> Total deployments:   2
> Final cost:          0.02334582 ETH
```
What's important is the `contract address` of `RelaySet`, which is `0x6f65d8841ef120D8B58b3494aB0f5f592CB9d246`. Now we repeat the same step to deploy `RelayedOwnedSet`. Create `3_owned_deploy.js` using the following snippet under `/migrations`:
```
var RelayedOwnedSet = artifacts.require("./RelayedOwnedSet.sol");
module.exports = function(deployer){
        deployer.deploy(RelayedOwnedSet, "0x6f65d8841ef120D8B58b3494aB0f5f592CB9d246", ["0xa2922fec00bb29fe13d68e87f64b9ad1719ed64a"]);
};
```
Notice that this time, we pass two arguments: the `RelaySet` contract address and the initial set of validators we used for our chain. Again, run `$ truffle migrate` and we should see something like this:
```
Compiling your contracts...
===========================
> Everything is up to date, there is nothing to compile.

Starting migrations...
======================
> Network name:    'development'
> Network id:      8995
> Block gas limit: 0x7a1200

3_owned_deploy.js
=================

   Deploying 'RelayedOwnedSet'
   ---------------------------
   > transaction hash:    0x45eb1989785683c0732e509911fda9847c3bad70aeb56d69a2b2d545856c74e6
   > Blocks: 0            Seconds: 8
   > contract address:    0xe6aB51C1726417BF33A05b6C16aca8eDa61e0B06
   > block number:        6356
   > block timestamp:     1562785995
   > account:             0x12db1ee91481573302f63ebf3d735820081c68a2
   > balance:             9999.843493299999999985
   > gas used:            1952182
   > gas price:           20 gwei
   > value sent:          0 ETH
   > total cost:          0.03904364 ETH

   > Saving migration to chain.
   > Saving artifacts
   -------------------------------------
   > Total cost:          0.03904364 ETH

Summary
=======
> Total deployments:   1
> Final cost:          0.03904364 ETH
```
Notice how Truffle automatically manages what was deployed before and what is new. Once again, keep the `contract address` of `RelayedOwnedSet`, which is `0xe6aB51C1726417BF33A05b6C16aca8eDa61e0B06`. Now we need to link `RelaySet` contract to `RelayedOwnedSet` contract.  

We can invoke a contract function via web3.js API, but in order to do so, we need to have contract Application Binary Interface (ABI). The ABI of each contract can be stored under `/abis`. We are going to use `RelaySet.json` first. For now, launch a Node console and create web3 object as explained in section 3. We will create our contract object like the following:
```
> const relay = new web3.eth.Contract([...], "0x6f65d8841ef120D8B58b3494aB0f5f592CB9d246");
undefined
> relay.methods.setRelayed("0xe6aB51C1726417BF33A05b6C16aca8eDa61e0B06").send({from: "0x12db1ee91481573302f63ebf3d735820081c68a2"}).then(console.log);
```
We use the ABI of `RelaySet` contract as the first argument, and the second argument is the contract address of `RelaySet`. Using the contract object, we can call `setRelayed` function to connect `RelaySet` to `RelayedOwnedSet`. The first argument is the contract address of `RelayedOwnedSet`, and `from` is the address of account where the contract is being called. In this case, we use the address of node1. Then we should see something like the following:
```
Promise {
  _bitField: 0,
  _fulfillmentHandler0: undefined,
  _rejectionHandler0: undefined,
  _promise0: undefined,
  _receiver0: undefined }
> { blockHash: '0xa3b983ce0188987f6e7dea4245320adbec194ba51ca74d9fc114f3dd40d9326f',
  blockNumber: 2226,
  contractAddress: null,
  cumulativeGasUsed: 45333,
  from: '0x12db1ee91481573302f63ebf3d735820081c68a2',
  gasUsed: 45333,
  logsBloom: '0x00000000000000000000000000000000000000000000000000000000000000000002000102000000000000000000000000000000000002000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000020000000000000000000800000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000040000000000000000000000000000000000000004000000000000000800000000000000000000000000000000000000000008000000000000020000000000000000020000000000000000000000000000000000000000000000000',
  root: null,
  status: '0x1',
  to: '0x6f65d8841ef120D8B58b3494aB0f5f592CB9d246',
  transactionHash: '0xd8d96a8832afa006f69accb51cc689109e0fed19c635745165257bb52d431252',
  transactionIndex: 0,
  events:
   { NewRelayed:
      { address: '0x6f65d8841ef120D8B58b3494aB0f5f592CB9d246',
        blockHash: '0xa3b983ce0188987f6e7dea4245320adbec194ba51ca74d9fc114f3dd40d9326f',
        blockNumber: 2226,
        logIndex: 0,
        removed: false,
        transactionHash: '0xd8d96a8832afa006f69accb51cc689109e0fed19c635745165257bb52d431252',
        transactionIndex: 0,
        transactionLogIndex: '0x0',
        type: 'mined',
        id: 'log_f3d29c65',
        returnValues: [Object],
        event: 'NewRelayed',
        signature: '0x4fea88aaf04c303804bb211ecc32a00ac8e5f0656bb854cad8a4a2e438256b74',
        raw: [Object] } } }
```
This is the end the deploy process. 

##### # Modifying genesis block for a hard fork
Once the contracts are deployed, stop the nodes and modify `poa_demo.json` to get ready for a hard fork. We are going to modify `"validators"` section to the following:
```
"validators" : {
    "multi": {
        "0": {
            "list": ["0xa2922fec00bb29fe13d68e87f64b9ad1719ed64a"]
        },
        "2300": {
                "contract": "0x6f65d8841ef120D8B58b3494aB0f5f592CB9d246"
        }
    }
}
```
What this means is that from the genesis block up to block #2299, there was only one validator (node0). However, from block #2300, validators are going to be determined by the contract at that address (RelaySet). It is important that we choose a block number that's in the future. Once we save the change, distribute the file to every node and restart the nodes. When the network reaches the specified block number, we should see something like this:
```
2019-06-28 14:34:35  Imported #2299 0x65e0…1081 (0 txs, 0.00 Mgas, 30 ms, 0.57 KiB)
2019-06-28 14:34:40  Signal for switch to contract-based validator set.
2019-06-28 14:34:40  Initial contract validators: [0xa2922fec00bb29fe13d68e87f64b9ad1719ed64a]
2019-06-28 14:34:40  Applying validator set change signalled at block 2300
2019-06-28 14:34:40  Imported #2300 0xbb59…2dd9 (0 txs, 0.00 Mgas, 27 ms, 0.57 KiB)
2019-06-28 14:34:45  Imported #2301 0x6446…1c2c (0 txs, 0.00 Mgas, 8 ms, 0.57 KiB)
```

##### # Adding a new validator
Now that the validators are not based on the initially set fixed list any more, we can invoke `addValidator` functions via web3.js API to add a new validator. As we did previously, create web3 object. This time, we will create `RelayedOwnedSet` object using corresponding ABI and address. Then we can directly call `addValidator` function with a desired validator's address. Let's say we want to add node1 as a second validator like the following:
```
> const owned = new web3.eth.Contract([...], "0xe6aB51C1726417BF33A05b6C16aca8eDa61e0B06");
undefined
> owned.methods.addValidator("0x12db1ee91481573302f63ebf3d735820081c68a2").send({from: "0x12db1ee91481573302f63ebf3d735820081c68a2"}).then(console.log);
Promise {
  _bitField: 0,
  _fulfillmentHandler0: undefined,
  _rejectionHandler0: undefined,
  _promise0: undefined,
  _receiver0: undefined }
> { blockHash: '0x55d288e243d14058e614f7e03ce80a2eb62d63b9634e5583d39fac00a921e656',
  blockNumber: 2374,
  contractAddress: null,
  cumulativeGasUsed: 101475,
  from: '0x12db1ee91481573302f63ebf3d735820081c68a2',
  gasUsed: 101475,
  logsBloom: '0x00000000000000000000000000000000000000000000000000000000000000000000000002000000000000000000000000000000000002000000008000000000000000000000000000000000000000000000000000000000000000200000000020000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000080000000000000000000000000000006000000000000000000000000000000000000000000000000000000000008000000000000000000000000000000000000000000000000000000000000000000000000000000000',
  root: null,
  status: '0x1',
  to: '0xe6aB51C1726417BF33A05b6C16aca8eDa61e0B06',
  transactionHash: '0x994014c8b4a5ded637206dadc9ba371d8b93b15b901a4a26388b57918135ce22',
  transactionIndex: 0,
  events:
   { '0':
      { address: '0x6f65d8841ef120D8B58b3494aB0f5f592CB9d246',
        blockHash: '0x55d288e243d14058e614f7e03ce80a2eb62d63b9634e5583d39fac00a921e656',
        blockNumber: 2374,
        logIndex: 0,
        removed: false,
        transactionHash: '0x994014c8b4a5ded637206dadc9ba371d8b93b15b901a4a26388b57918135ce22',
        transactionIndex: 0,
        transactionLogIndex: '0x0',
        type: 'mined',
        id: 'log_4ebb330d',
        returnValues: Result {},
        event: undefined,
        signature: null,
        raw: [Object] } } }
```
When the transaction is sent, we should see something like the following, indicating that the address of node1 is now added to the validator list, which is managed by the contract:
```
2019-06-28 14:40:45  Imported #2373 0x7f70…a6e7 (0 txs, 0.00 Mgas, 3 ms, 0.57 KiB)
2019-06-28 14:40:46  eth_sendTransaction is deprecated and will be removed in future versions: Account management is being phased out see #9997 for alternatives.
2019-06-28 14:40:50  Signal for transition within contract. New list: [0xa2922fec00bb29fe13d68e87f64b9ad1719ed64a, 0x12db1ee91481573302f63ebf3d735820081c68a2]
2019-06-28 14:40:50  Applying validator set change signalled at block 2374
2019-06-28 14:40:50  Transaction mined (hash 0x994014c8b4a5ded637206dadc9ba371d8b93b15b901a4a26388b57918135ce22)
2019-06-28 14:40:50  Imported #2374 0x55d2…e656 (1 txs, 0.10 Mgas, 7 ms, 0.71 KiB)
```
Now that node1 is also a validator, don't forget adding `[mining]` section to the `node1.toml`. Now we can add a new validator by invoking `addValidator` any time. 
