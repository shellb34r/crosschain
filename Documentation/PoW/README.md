# Ethereum PoW on RPi

The following was performed on a Raspberry Pi 3B+ running Ubuntu 18.04.2 . All instructions are to be entered on Pis.

Prerequisites: Go must be installed and run `sudo apt-get install -y build-essential`

### Downloading Geth
Geth is the command line interface for running a full ethereum node implemented in Go. More details can be found here: https://github.com/ethereum/go-ethereum. 

If working with a Raspberry Pi, we recommend adjusting the difficulty scaling of Ethereum to a fixed scale
so that mining blocks can occur aq  t a reasonable rate (otherwise it'd take hours to mine a block). We've included a compiled version of Geth for 
ARM64 which mines on the order of seconds. Consult PoW_difficulty.md if customization of difficulty or architecture is needed.

If you downloaded our version of Geth, simply run
 
 `sudo mv geth-linux-arm64 /usr/local/bin/geth`

If you would like to use the original Geth, run

```
cd ~/Downloads
wget https://gethstore.blob.core.windows.net/builds/geth-linux-arm64-1.8.27-4bcc0a37.tar.gz
tar -xzf geth-linux-arm64-1.8.27-4bcc0a37.tar.gz
cd geth-linux-arm64-1.8.27-4bcc0a37/
sudo mv geth /usr/local/bin 
```

A successful geth installation can be checked with `geth version`.

### Configuring a private chain
First enter the correct folder for use with CrossChain: `cd ~/CrossChain/PoW`

It is required to create a new folder to contain the private chain. Initial configurations of the chain is initialized in a file called `genesis.json`. A barebones configuration is shown below:
```
# ~/CrossChain/PoW/$YOURCHAINNAME/genesis.json
{
    "config":{
        "chainId":8888,
        "homesteadBlock":0,
        "eip155Block":0,
        "eip158Block":0
    },
    "difficulty":"0x0",
    "gasLimit":"2100000000",
    "alloc":{}
}
```
The difficulty and gaslimit may be adjusted. Accounts can be prepopulated by filling in the alloc and additional parameters include coinbase, extraData, nonce, mixhash, parentHash. More information can be found here: https://github.com/ethereum/go-ethereum#defining-the-private-genesis-state  

The chain can now be initialized a new account can be created. The $UNIQUE_NAME is a arbitrary name that should be unique for each node. The $DIRECTORY_PATH should be ~/CrossChain/PoW/$YOURCHAINNAME
```
geth --identity $UNIQUE_NAME --datadir $DIRECTORY_PATH init genesis.json 
geth --identity $UNIQUE_NAME --datadir $DIRECTORY_PATH account new
```  
This must be done on all nodes.

### Joining multiple nodes onto the chain
Geth's Javascript console is accessed to obtain enode IDS for each node.
Be sure to fill in the identity, rpcaddr, and datadir field, the $IP_ADDRESS is the ip address of the pi the command is being run on: 

```
geth --identity $UNIQUE_NAME --allow-insecure-unlock --rpc --rpcport 8546 --rpcaddr $IP_ADDRESS --datadir $DIRECTORY_PATH --port 30303 --networkid 555 --rpccorsdomain "*" --rpcapi "eth,net,web3,miner,debug,personal,rpc" console
```  
The network id and rpcport can be changed, but make sure it is the same through all nodes. The ports chosen were arbitrary, more options can be found here: https://github.com/ethereum/go-ethereum/wiki/Command-Line-Options. 

It should lead to a javascript console. In the console,   
```
admin.nodeInfo.enode
```
The output should look something similar to
```
"enode://394c2efd13999219c7133abc80148839c5e724a664d2df026a89eb462874dc5a7580e36d7bea6af1e3abf19ee7250940b6e842f997acdc24ea90cba511a48ab3@127.0.0.1:30303?discport=0"
```

Repeat the above on all nodes to get the enode value for each node and omit `?discport=0` and replace the IP address with the appropriate value. 

For three nodes, a compilation of all three enodes may look like this. This will be used in the next section.
``` 
[
"enode://32415185f09c2ce5480ebed9627f5507f2cc5b437ee2d3b40dfc637beac63270a3c0035b982cb709b4aa6e351a9cd8b321a96feb7c089f6a01391ae09131ddf1@172.16.10.119:30303",
"enode://0b59ae91289417372155be3e44de01c3728ea72f99a115e1bee0b828715ea1e311427c89aca2ade7bfebd306559ed7e77051e0dc429a5893d896ce4456080aeb@172.16.10.99:30303",
"enode://f9294fb8af3093383b683227523404046dd939040337b01da797cb3d4309da7a22e5b10019968010477ae6ef6b498f46feea85ce222020604cf05f175bc628e8@172.16.9.235:30303"
]
```

Alternatively, nodes may be added in the Javascript console through the `admin.addPeer()` command. 

Successful peer connections can be checked with `admin.peers` in the console. 
If there are issues connecting the node or if there are unknown nodes displaying when `admin.addPeer() ` is run try using another networkid.
### Working with CrossChain
The configurations of the chain is outputed into a file so that it can be started remotely in CrossChain. 
On each Pi, run 
```
geth --identity $UNIQUE_NAME --allow-insecure-unlock --rpc --rpcport 8546 --rpcaddr $IP_ADDRESS --datadir $DIRECTORY_PATH --port 30303 --networkid 555 --rpccorsdomain "*" --rpcapi "eth,net,web3,miner,debug,personal,rpc" dumpconfig > config.toml
```  

If multiple nodes are to be connected, insert the snippet from the previous section regarding all the enodes into `config.toml`
where it says "StaticNodes".

Then create a start script `run.sh`, the permissions may need to be changed to executable for all
```
# run.sh
geth --config $FULLPATH_CONFIG.TOML 
```
where $FULLPATH_CONFIG is the absolute path of the config.toml file that should have been created. This needs to be done for all nodes.

##### Mining from Console

To mine, run `miner.start()` in the console. Addition commands can be found here: https://ethereum.gitbooks.io/frontier-guide/content/rpc.html. 

##### Get Address from Console
From geth console:  
`eth.coinbase`

##### Checking Balance from Console
From geth console:  
`web3.fromWei(eth.getBalance(eth.coinbase), "ether")`

##### Transferring Coins from Console 
From geth console:  
1. `personal.unlockAccount(eth.coinbase, "$password")`  
    if there is a HTTP problem, add "--allow-insecure-unlock to geth initialization"
2. `eth.sendTransaction({from:eth.coinbase, to:$address, value:web3.toWei($amount,"ether)})`
3. `miner.start()` to sync


