# How to adjust the difficulty (PoW)

This guide shows how to reduce the difficulty of PoW on Ethereum so that we can utilize the mining feature on RPi. Since we need to modify a line of code, we need to utilize the cross-compiling feature that Geth provides. The guide is based on [a blog post](https://hackernoon.com/how-to-reduce-block-difficulty-in-ethereum-private-testnet-2ad505609e82) and the [official Wiki](https://github.com/ethereum/go-ethereum/wiki/Cross-compiling-Ethereum).

### Prerequisites
Compiling Geth requires Go language and Docker. Installing Go and Docker is trivial. Once everything is set up, make sure the Docker Engine is running, and the Go's PATH is configured. 

### Build
1. Clone the official Go-Ethereum
`$ git clone https://github.com/ethereum/go-ethereum`

2. Modify `consensus.go` under `consensus/ethash/`
Look for the line `return CalcDifficulty(chain.Config(), time, parent)`
Replace the line with `return big.NewInt(0x50)`  

- What this does is that it changes the difficulty of mining to a fixed number (0x50). Keep in mind that this is a heuristic value that a miner mines roughly one block for 10 seconds on average. 

- Also keep in mind that when we apply this change, we notice that the total difficulty is increasing by a multiple of 0x50 for each mined block. However, the actual difficulty is a constant (0x50), thus the block generation rate stays the same. 

3. Within the `go-ethereum` directory, run `make geth-linux-arm64`. This can take a while. Note that the ARM64
architecture is for Raspberry Pi running Ubuntu, if other architectures are needed, look here :https://github.com/ethereum/go-ethereum/wiki/Cross-compiling-Ethereum. 
Unfortunately, at the time of writing Geth does not support 32-bit architectures.

4. Copy the modified executable to be used as `geth`
`$ sudo cp build/bin/geth-linux-arm64 /usr/local/bin/geth`

4. Save the following snippet as `genesis.json`, which will be used for testing:
```
{
   "config": {
      "chainId": 1994,
      "homesteadBlock": 0,
      "eip155Block": 0,
      "eip158Block": 0,
      "byzantiumBlock": 0
   },
   "difficulty": "0x0",
   "gasLimit": "0x8000000",
   "alloc": {}
}
```

5. Initialize a private chain and a new account using `genesis.json` and start mining to see RPi mining. While the elapsed time for each mined block can fluctuate, the rate is about `1 block / 10 sec` on average. 
