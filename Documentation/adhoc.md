1. `sudo vim /etc/network/interfaces`
source-directory /etc/network/interfaces.d  

auto lo  
iface lo inet loopback  
iface eth0 inet dhcp  

allow-hotplug wlan0  
iface wlan0 inet static  
	address 192.168.0.1  #increment this according to additional nodes (192.168.0.2...)  
	netmask 255.255.255.0  
	network 192.168.0.0  
	broadcast 192.168.0.11  
wireless-essid ad-hoc-0  
wireless-mode ad-hoc  
wireless-channel 3  

2. reboot
3. `sudo ifdown --force wlan0`
4. `sudo ifdown --force eth0`
5. `sudo ifup wlan0`
6. `sudo ifup eth0`

If there is a problem with rfkill, `sudo rfkill unblock all`.
can test by pinging ip
