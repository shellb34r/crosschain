#Installing Docker and Docker-Compose

1. `sudo apt-get install apt-transport-https ca-certificates software-properties-common -y`
2. `curl -fsSL get.docker.com -o get-docker.sh && sh get-docker.sh`
3. `sudo usermod -aG docker pi`
4. `sudo curl https://download.docker.com/linux/raspbian/gpg`
5. `vim /etc/apt/sources.list`  
    add the following line: `deb https://download.docker.com/linux/raspbian/ stretch stable`
6. `sudo apt-get update`
7. `sudo apt-get upgrade`
8. `systemctl start docker.service`
9. `docker info`
10. `sudo apt-get -y install python-setuptools && sudo easy_install pip  && sudo pip install docker-compose`