
#%%
def jitter(latencyArr):
    past = None
    diffArr = []
    for latency in latencyArr:
        if past != None: 
            diff = abs(latency - past)
            print(diff)
            diffArr.append(diff)
        past = latency
    return sum(diffArr)/ len(diffArr)

#%%
# somehow I cannot install paramiko, so I will just use a fake one. 
filename = "./GUI/fakeoutput.txt"
stdout = open(filename, "r")
lc = 0
# requestCount: parameter of request count 
requestCount = 10
latencyArr = [None] * requestCount
#%%
for line in stdout.readlines():
    if lc >= 1 and lc <= requestCount:
        wordList = line.split(" ")
        # print(wordList)
        latency = float(wordList[6].lstrip("time="))
        print(latency)
        latencyArr[lc - 1] = latency
    lc += 1
#%%
averageLatency = sum(latencyArr) / len(latencyArr)
averageJitter = jitter(latencyArr)