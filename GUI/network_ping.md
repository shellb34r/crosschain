# Explanation of PING command
```
ping -c $requestcount(int) -i $interval_of_requests(int) $ip_address
```
Pass the stdout result of this command to ```stdout``` variable, and ```$requestcount(int)``` to ```requestCount```. We can discuss the values later, but the longer ```$requestcount(int)*$interval_of_requests(int)```, the longer it takes to get back the result. 