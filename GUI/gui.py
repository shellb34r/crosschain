# DEPENDENCIES:
# 임 sudo apt-get install bwm-ng


# from fbs_runtime.application_context.PyQt5 import ApplicationContext
from PyQt5.QtCore import QDateTime, Qt, QTimer, QThread, pyqtSignal, pyqtSlot, QObject
from PyQt5.QtWidgets import (QApplication, QCheckBox, QComboBox, QDateTimeEdit,
                             QDial, QDialog, QGridLayout, QGroupBox, QHBoxLayout, QLabel, QLineEdit,
                             QProgressBar, QPushButton, QRadioButton, QScrollBar, QSizePolicy,
                             QSlider, QSpinBox, QStyleFactory, QTableWidget, QTabWidget, QTextEdit,
                             QVBoxLayout, QWidget, QMessageBox, QInputDialog, QFormLayout, QScrollArea)
import pyqtgraph as pg
import sys
import paramiko
import socket
from web3 import Web3, HTTPProvider
import time
import os
import csv
import re
import sqlite3


class BlockchainIoT(QDialog):
    # Initializes entire window
    def __init__(self, parent=None):
        super(BlockchainIoT, self).__init__(parent)

        self.originalPalette = QApplication.palette()
        self.left = 10
        self.top = 10
        self.width = 1000
        self.height = 700
        self.initUI()
        self.sample_rate = 5000
        self.number = 0
        self.last_block_time = 0
        self.graph_x = []
        self.graph_y = []

        self.nodes = []
        self.connected_nodes = {}  # name: node
        self.ips = []
        self.block_data_param = ['Number', 'Difficulty', 'Number of Transactions', 'Size of Block',
                                 'Time Since Last Block']

        self.attribute = None
        self.consensus = 'PoW'
        self.log_loc = '../logs/'
        self.left_layout = None
        self.nodes_group_box = None
        self.nodes_layout = None
        self.print_console = None
        self.node_scroll = None
        self.right_layout = None
        self.helper = None
        self.right_group_box = None
        self.left_group_box = None
        self.plot = None
        self.legend = None
        self.sample_entry = None
        self.log_data = True
        self.realtime = None
        self.sample_label = None

        self.main_layout = QGridLayout()
        # self.main_layout.setColumnStretch(3, 4)

        self.add_node_button = QPushButton("Add a node")
        self.add_node_button.setDefault(True)
        self.add_node_button.clicked.connect(self.add_node_button_clicked)
        self.main_layout.addWidget(self.add_node_button, 0, 0)

        self.select_node_button = QPushButton("Select a node")
        self.select_node_button.setDefault(True)
        self.select_node_button.clicked.connect(self.select_node_button_clicked)
        self.main_layout.addWidget(self.select_node_button, 1, 0)

        consensus_drop_down = QComboBox()
        consensus_drop_down.addItems(['PoW', 'PoS', 'PoA'])
        consensus_drop_down.activated[str].connect(self.set_consensus)
        self.main_layout.addWidget(consensus_drop_down, 0, 2)

        self.log_loc_entry = QLineEdit()
        self.log_loc_entry.setPlaceholderText('Log Location')
        self.log_loc_entry.setMaximumWidth(250)
        self.main_layout.addWidget(self.log_loc_entry, 0, 3, 1, 1)

        log_loc_button = QPushButton("Set Loc")
        log_loc_button.setMaximumWidth(100)
        log_loc_button.clicked.connect(self.set_log_loc)
        self.main_layout.addWidget(log_loc_button, 0, 4, 1, 1)

        log_toggle_button = QPushButton("Log Data")
        log_toggle_button.setCheckable(True)
        log_toggle_button.setChecked(True)
        log_toggle_button.clicked.connect(self.log_toggle)
        self.main_layout.addWidget(log_toggle_button, 0, 5)

        # left group lists nodes
        # right group displays data
        self.create_left_group_box()
        self.create_right_group_box()

        self.main_layout.addWidget(self.left_group_box, 2, 0)
        self.main_layout.addWidget(self.right_group_box, 2, 2, 1, 4)
        self.setLayout(self.main_layout)

        self.timer = QTimer()
        self.timer.timeout.connect(self.sampler)
        self.timer.start(self.sample_rate)

    def initUI(self):
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.setWindowTitle("Blockchain for IoT")
        QApplication.setStyle(QStyleFactory.create('Fusion'))

    # when "add node" is clicked, calls AddNode() which creates a window to prompt input to create a new node
    def add_node_button_clicked(self):
        add_node = AddNode(self)
        add_node.show()

    def select_node_button_clicked(self):
        self.db = DBConnection()
        self.db.cursor.execute('SELECT * FROM infos')
        credentials = self.db.cursor.fetchall()
        if not credentials:
            self.db.conn.close()
            return
        self.select_node_button.deleteLater()
        cred_drop_down = QComboBox()
        cred_drop_down.addItem("Select a node")

        for cred in credentials:
            node = cred[0] + "@" + cred[1]
            cred_drop_down.addItem(node)
        
        self.main_layout.addWidget(cred_drop_down, 1, 0)
        cred_drop_down.activated[str].connect(self.select_node_creater)

    def select_node_creater(self, node):
        info = node.split('@')
        if info[0] == 'Select a node':
            return

        self.db.cursor.execute('SELECT password FROM infos WHERE username = ? AND hostname = ?', (info[0], info[1]))
        pwd = self.db.cursor.fetchone()[0]

        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            ssh_client.connect(hostname=info[1], username=info[0], password=pwd)
            self.add_node(node, ssh_client)
            self.main_layout.itemAt(7).widget().setCurrentIndex(0)
        except Exception as e:
            print(e)

    def set_log_loc(self):
        self.log_loc = self.log_loc_entry.text()
        self.print_console.append("Log location set: " + self.log_loc)

    def log_toggle(self):
        if self.log_data is True:
            self.log_data = False
        else:
            self.log_data = True

        self.print_console.append(str(self.log_data) + "\n")

    # adds node object to display
    def add_node(self, node_name, ssh_client):
        node = Node(self, node_name, ssh_client, self.number)
        self.number = self.number + 1
        self.nodes.append(node)
        self.nodes_layout.addWidget(node)

    def graph_past(self):
        self.past_graph = PastGraph(self.log_loc)
        self.past_graph.show()

    def create_left_group_box(self):
        self.left_group_box = QGroupBox()
        self.left_group_box.setMinimumWidth(400)
        self.left_group_box.setMaximumWidth(400)
        self.left_layout = QGridLayout()
        self.nodes_group_box = QGroupBox()
        self.nodes_layout = QGridLayout()
        self.nodes_group_box.setLayout(self.nodes_layout)
        self.print_console = QTextEdit()
        self.print_console.setReadOnly(True)
        self.node_scroll = QScrollArea()
        self.node_scroll.setWidget(self.nodes_group_box)
        self.node_scroll.setWidgetResizable(True)
        self.left_layout.addWidget(self.node_scroll, 0, 0, 1, 1)
        self.left_layout.addWidget(self.print_console, 1, 0, 1, 1)
        self.left_layout.setRowStretch(0, 2)
        self.left_layout.setRowStretch(1, 1)
        self.left_group_box.setLayout(self.left_layout)

    def create_right_group_box(self):
        self.right_group_box = QGroupBox()
        self.right_group_box.setMinimumWidth(650)
        self.right_layout = QGridLayout()

        attribute_drop_down = QComboBox()
        attribute_drop_down.addItems(['Number', 'Difficulty', 'Number of Transactions', 'Size of Block',
                                      'Time Since Last Block', 'CPU Usage', 'Network Bandwidth Out',
                                      'Network Bandwidth In', 'Network Packet Out', 'Network Packet In', 'Avg Latency',
                                      'Jitter'])
        attribute_drop_down.activated[str].connect(self.set_attribute)
        self.right_layout.addWidget(attribute_drop_down, 0, 0, 1, 1)

        self.sample_entry = QLineEdit()
        self.sample_entry.setPlaceholderText('Sample Rate')
        self.right_layout.addWidget(self.sample_entry, 0, 1, 1, 1)

        sample_button = QPushButton("Set Rate")
        sample_button.clicked.connect(self.set_sample, False)
        self.right_layout.addWidget(sample_button, 0, 2, 1, 1)

        self.sample_label = QLabel('Sample Rate: ' + str(self.sample_rate) + 'ms')
        self.right_layout.addWidget(self.sample_label, 0, 3, 1, 1)

        graph_past_button = QPushButton("Graph Past Button")
        graph_past_button.clicked.connect(self.graph_past)
        self.right_layout.addWidget(graph_past_button, 0, 4, 1, 1)

        axis = DateAxis(orientation='bottom')
        pg.setConfigOption('background', 'w')
        self.plot = pg.PlotWidget(axisItems={'bottom': axis})
        self.plot.setMinimumWidth(800)
        self.legend = self.plot.addLegend()
        self.legend.setMaximumWidth(175)
        self.plot.setLabel('bottom', 'Time', '')

        self.right_layout.addWidget(self.plot, 1, 0, 1, 5)

        self.right_group_box.setLayout(self.right_layout)

    def set_attribute(self, attribute):
        self.attribute = attribute
        self.plot.clear()
        self.graph_x.clear()
        self.graph_y.clear()
        for node in self.connected_nodes.values():
            node.graph_x.clear()
            node.graph_y.clear()

        if attribute in self.block_data_param:
            self.realtime = False
        else:
            self.realtime = True

        if attribute == "Time Since Last Block":
            self.plot = pg.PlotWidget()
            self.plot.setMinimumWidth(1100)
            self.plot.setMinimumHeight(600)
            self.right_layout.addWidget(self.plot, 1, 0, 1, 5)
            self.plot.setLabel('bottom', 'Block Number', '')
            self.plot.setLabel('left', 'Time From Last Block', 's')
        else:
            axis = DateAxis(orientation='bottom')
            pg.setConfigOption('background', 'w')
            self.plot = pg.PlotWidget(axisItems={'bottom': axis})
            self.plot.setMinimumWidth(1100)
            self.legend = self.plot.addLegend()
            self.legend.setMaximumWidth(175)
            self.right_layout.addWidget(self.plot, 1, 0, 1, 5)
            self.plot.setLabel('bottom', 'Time', '')

        self.print_console.append("Attribute set: " + str(self.attribute) + "\n")

    # if switching consensus, need to disconnect from whatever API and switch to a new one
    def set_consensus(self, consensus):
        old_consensus = self.consensus
        self.consensus = consensus

        if self.connected_nodes:
            for node in self.connected_nodes.values():
                if old_consensus == "PoW":
                    node.ssh_client.exec_command("killall geth")
                elif old_consensus == "PoS":
                    node.ssh_client.exec_command("killall qtum")
                elif old_consensus == "PoA":
                    node.ssh_client.exec_command("killall parity")
                node.graph_x.clear()
                node.graph_y.clear()

            if consensus == "PoW" or consensus == "PoA":
                self.helper = SwitchHelper(self)
                self.helper.show()
            elif self.consensus == "PoS":
                for node in self.connected_nodes.values():
                    node.start_chain()

        self.print_console.append("Consensus set: " + str(self.consensus) + "\n")

    def switch(self, name, port):
        for node in self.connected_nodes.values():
            node.chain_name = name
            node.port = str(port)
            node.start_chain()

    def set_sample(self, sample_bool):
        if int(self.sample_entry.text()) < 2500 and sample_bool is False:
            self.warning = SampleWarning(self)
            self.warning.show()

        else:
            self.sample_rate = int(self.sample_entry.text())
            self.timer.stop()
            self.timer = QTimer()
            self.timer.timeout.connect(self.sampler)
            self.timer.start(self.sample_rate)

            self.sample_label.setText("Sample Rate: " + str(self.sample_rate))

        self.print_console.append("Sample Rate Set: " + str(self.sample_rate) + "\n")

    def add_data(self, node):
        try:
            if self.attribute == 'CPU Usage':
                self.plot.setLabel('left', 'CPU Usage', '%')
                node.graph_x.append(node.netsys_data['timestamp'])
                node.graph_y.append(node.netsys_data['cpu usage'])

            elif self.attribute == 'Network Bandwidth Out':
                self.plot.setLabel('left', 'Network Bandwidth Out', 'Bytes/s')
                node.graph_x.append(node.netsys_data['timestamp'])
                node.graph_y.append(node.netsys_data['bytes out/s'])

            elif self.attribute == 'Network Bandwidth In':
                self.plot.setLabel('left', 'Network Bandwidth In', 'Bytes/s')
                node.graph_x.append(node.netsys_data['timestamp'])
                node.graph_y.append(node.netsys_data['bytes in/s'])

            elif self.attribute == 'Network Packet Out':
                self.plot.setLabel('left', 'Network Packets Out', 'Packets/s')
                node.graph_x.append(node.netsys_data['timestamp'])
                node.graph_y.append(node.netsys_data['packets out/s'])

            elif self.attribute == 'Network Packet In':
                self.plot.setLabel('left', 'Network Packets In', 'Packets/s')
                node.graph_x.append(node.netsys_data['timestamp'])
                node.graph_y.append(node.netsys_data['packets in/s'])

            elif self.attribute == 'Jitter':
                self.plot.setLabel('left', 'Avg Jitter', 's')
                node.graph_x.append(node.netsys_data['timestamp'])
                node.graph_y.append(node.netsys_data['Avg Jitter'])

            elif self.attribute == 'Avg Latency':
                self.plot.setLabel('left', 'Avg Latency', 's')
                node.graph_x.append(node.netsys_data['timestamp'])
                node.graph_y.append(node.netsys_data['Avg Latency'])

            elif self.attribute == 'Difficulty':
                self.plot.setLabel('left', 'Difficulty', '')
                node.graph_x.append(node.latest_block['timestamp'])
                node.graph_y.append(node.latest_block['difficulty'])

            elif self.attribute == 'Number of Transactions':
                self.plot.setLabel('left', '# Transactions per Block', '')
                node.graph_x.append(node.latest_block['timestamp'])
                node.graph_y.append(node.latest_block['number_transactions'])

            elif self.attribute == 'Size of Block':
                self.plot.setLabel('left', 'Size', 'Bytes')
                node.graph_x.append(node.latest_block['timestamp'])
                node.graph_y.append(node.latest_block['size'])

            elif self.attribute == 'Number':
                self.plot.setLabel('left', 'Number of Blocks', '')
                node.graph_x.append(node.latest_block['timestamp'])
                node.graph_y.append(node.latest_block['number'])

            elif self.attribute == 'Time Since Last Block':
                self.plot.setLabel('left', 'Time Since Last Block', 's')
                self.graph_x.append(node.latest_block['number'])
                self.graph_y.append(node.latest_block['time since last block'])

        except TypeError:
            del node.graph_x[-1]
            self.print_console.append('Missing Data: ' + str(node.name) + "\n")

        self.grapher()

    # plots based on timer
    def grapher(self):
        self.plot.clear()

        if self.attribute == 'Time Since Last Block':
            plot_item = self.plot.plot(self.graph_x[1:], self.graph_y[1:], pen='b', symbolBrush='b')

        else: 
            for node in self.connected_nodes.values():
                self.legend.removeItem(node.name)

                plot_item = self.plot.plot(node.graph_x, node.graph_y, pen=pg.intColor(node.number),
                                           symbolBrush=pg.intColor(node.number))
                self.legend.addItem(plot_item, node.name)

    def sampler(self):
        for node in self.connected_nodes.values():
            node.sample_netsys()

    def log_block(self, name, block):
        if self.log_data is True:
            data = [self.consensus, block['number'], block['timestamp'], block['difficulty'],
                    block['number_transactions'], block['size'], block['time since last block']]

            filename = self.log_loc + name + '_' + self.consensus + '_Blocks.csv'
            try:
                if not os.path.isfile(filename):
                    with open(filename, 'a+') as file:
                        writer = csv.writer(file)

                        writer.writerow(['Consensus', 'Number', 'Timestamp', 'Difficulty', 'Number of Transactions',
                                         'Size of Block', 'Time Since Last Block'])

                with open(filename, 'a+') as file:
                    writer = csv.writer(file)
                    writer.writerow(data)

            except Exception:
                self.file_error = FileError(filename)
                self.file_error.show()

    def log_netsys(self, netsys, name):
        if self.log_data is True:
            data = [netsys['timestamp'], netsys['network name'], netsys['cpu usage'],
                    netsys['bytes out/s'], netsys['bytes in/s'], netsys['packets out/s'], netsys['packets in/s'],
                    netsys['Avg Latency'], netsys['Avg Jitter']]

            filename = self.log_loc + name + '_' + self.consensus + '_NetSys.csv'
            try:
                if not os.path.isfile(filename):
                    with open(filename, 'a+') as file:
                        writer = csv.writer(file)

                        writer.writerow(['Timestamp', 'Network Name', 'CPU Usage', 'Network Bandwidth Out',
                                         'Network Bandwidth In', 'Network Packet Out', 'Network Packet In', 'Avg Latency',
                                         'Jitter'])

                with open(filename, 'a+') as file:
                    writer = csv.writer(file)
                    writer.writerow(data)

            except Exception:
                self.file_error = FileError(filename)
                self.file_error.show()


# Creates a Window which prompts input to connect to a node via SSH
class AddNode(QDialog):
    def __init__(self, parent):
        super(AddNode, self).__init__(parent)
        self.BlockchainIoT = parent

        self.username = QLineEdit(self)
        self.username_label = QLabel("Username")

        self.hostname = QLineEdit(self)
        self.hostname_label = QLabel("Hostname")

        self.password = QLineEdit(self)
        self.password.setEchoMode(QLineEdit.Password)
        self.password_label = QLabel("Password")

        self.btn_submit = QPushButton("Add Node")
        self.btn_submit.clicked.connect(self.submit_btn)

        self.layout = QFormLayout()
        self.layout.addRow(self.username_label, self.username)
        self.layout.addRow(self.hostname_label, self.hostname)
        self.layout.addRow(self.password_label, self.password)
        self.layout.addRow(self.btn_submit)

        self.setLayout(self.layout)

        self.error_label = QLabel('Authentication error')

    # utilizes paramiko package to connect via ssh, if successful creates a Node Object
    def submit_btn(self):
        username = self.username.text()
        hostname = self.hostname.text()
        password = self.password.text()

        ssh_client = paramiko.SSHClient()
        ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        try:
            ssh_client.connect(hostname=hostname, username=username, password=password)
            node_name = username + "@" + hostname
            self.BlockchainIoT.add_node(node_name, ssh_client)
            
            self.db = DBConnection()
            params = (username, hostname, password)
            self.db.cursor.execute("INSERT OR IGNORE INTO infos VALUES (?, ?, ?)", params)
            self.db.conn.commit()
            self.db.cursor.execute('SELECT * FROM infos')
            self.db.conn.close()                        #### Should I close or keep it open?

            self.close()
        except Exception as e:
            self.layout.addRow(self.error_label)
            print(e)


# Node object
class Node(QWidget):
    def __init__(self, parent, name, ssh_client, number):
        super(Node, self).__init__(parent)
        self.name = name
        self.ssh_client = ssh_client
        self.number = number
        self.BlockchainIoT = parent
        self.port = None
        self.balance = "0"
        self.thread_num = 4
        self.ip_address = None
        self.web3 = None
        self.chain_name = None
        self.stop_thread = False
        self.node_address = None
        self.password = "ethereum"
        self.thread_entry = None
        self.thread_btn = None
        self.worker1 = None
        self.thread1 = None
        self.worker2 = None
        self.thread2 = None
        self.worker3 = None
        self.thread3 = None
        self.worker4 = None
        self.thread4 = None
        self.latest_block = None
        self.netsys_data = {}
        self.graph_x = []
        self.graph_y = []

        self.node_layout = QGridLayout(self)
        self.node_layout.setAlignment(Qt.AlignTop)

        self.name_label = QLabel(self.name)
        self.node_layout.addWidget(self.name_label, 0, 0, 1, 1)

        self.connect = QPushButton('Start Chain')
        self.connect.clicked.connect(self.start_chain)
        self.node_layout.addWidget(self.connect, 1, 0, 1, 3)
        self.connect.hide()

        self.remove = QPushButton('Disconnect')
        self.remove.clicked.connect(self.disconnect_ssh)
        self.node_layout.addWidget(self.remove, 1, 2, 1, 1)
        self.remove.hide()

        self.start_mining_btn = QPushButton('Start Mining')
        self.start_mining_btn.clicked.connect(self.start_mining)
        self.node_layout.addWidget(self.start_mining_btn, 1, 0, 1, 1)
        self.start_mining_btn.hide()

        self.stop_mining_btn = QPushButton('Stop Mining')
        self.stop_mining_btn.clicked.connect(self.stop_mining)
        self.node_layout.addWidget(self.stop_mining_btn, 1, 1, 1, 1)
        self.stop_mining_btn.hide()

        self.transact_amount = QLineEdit()
        self.transact_amount.setPlaceholderText('Amount')
        self.node_layout.addWidget(self.transact_amount, 2, 0)
        self.transact_amount.hide()

        self.transact_location = QLineEdit()
        self.transact_location.setPlaceholderText('Receiver')
        self.node_layout.addWidget(self.transact_location, 2, 1)
        self.transact_location.hide()

        self.transact_button = QPushButton('Make Transaction')
        self.transact_button.clicked.connect(self.transact)
        self.node_layout.addWidget(self.transact_button, 2, 2)
        self.transact_button.hide()

        if self.BlockchainIoT.consensus == "PoW" or self.BlockchainIoT.consensus == "PoA":
            self.chain_name_label = QLineEdit()
            self.chain_name_label.setPlaceholderText('Set Chain Name (required)')
            self.node_layout.addWidget(self.chain_name_label, 2, 0)

            self.chain_name_button = QPushButton("Set Chain Name")
            self.chain_name_button.clicked.connect(self.set_chain_name)
            self.node_layout.addWidget(self.chain_name_button, 2, 1)

            self.port_entry = QLineEdit()
            self.port_entry.setPlaceholderText('Set RPC Port (optional)')
            self.node_layout.addWidget(self.port_entry, 3, 0)

            self.port_button = QPushButton("Set Port")
            self.port_button.clicked.connect(self.set_port)
            self.node_layout.addWidget(self.port_button, 3, 1)

            if self.BlockchainIoT.consensus == "PoW":
                self.port = "8546"

            elif self.BlockchainIoT.consensus == "PoA":
                self.port = "8540"

            self.port_label = QLabel('Port: ' + self.port)
            self.node_layout.addWidget(self.port_label, 0, 1)

            self.balance_label = QLabel('Balance: ' + self.balance)
            self.node_layout.addWidget(self.balance_label, 0, 2)

        if self.BlockchainIoT.consensus == 'PoS':
            self.pos_block_num = -1

        if self.BlockchainIoT.consensus == 'PoW':
            self.pass_entry = QLineEdit()
            self.pass_entry.setPlaceholderText('Account Password')
            self.node_layout.addWidget(self.pass_entry, 4, 0)

            self.pass_button = QPushButton("Set Password")
            self.pass_button.clicked.connect(self.set_pass)
            self.node_layout.addWidget(self.pass_button, 4, 1)

    def set_chain_name(self):
        self.chain_name = str(self.chain_name_label.text())
        self.chain_name_label.hide()
        self.chain_name_button.hide()
        self.connect.show()

    # runs a script to start the chain at the specified directory
    def start_chain(self):
        # selected consensus is PoW, connect to ethereum
        if self.BlockchainIoT.consensus == 'PoW':
            start_command = "~/CrossChain/PoW/" + str(self.chain_name) + "/run.sh"
            stdin, stdout, stderr = self.ssh_client.exec_command(start_command)
            time.sleep(2)
            self.ip_address = socket.gethostbyname(self.name.partition('@')[2])
            self.web3 = Web3(HTTPProvider('http://' + self.ip_address + ':' + self.port))
            self.BlockchainIoT.connected_nodes[self.name] = self
            self.BlockchainIoT.print_console.append("Node " + self.name + " connected to " + str(self.chain_name) + '\n')

            self.node_address = self.web3.eth.coinbase
            self.web3.personal.unlockAccount(self.web3.toChecksumAddress(self.node_address), self.password, 0)
            self.balance = self.web3.fromWei(self.web3.eth.getBalance(self.node_address), "ether")
            self.balance_label.setText("Balance: " + str(self.balance))

            self.port_label.hide()
            self.port_button.hide()
            self.pass_entry.hide()
            self.pass_button.hide()

            self.thread_entry = QLineEdit()
            self.thread_entry.setPlaceholderText('Default Threads: 4')
            self.node_layout.addWidget(self.thread_entry, 3, 0)

            self.thread_btn = QPushButton("Set Thread #")
            self.thread_btn.clicked.connect(self.set_thread)
            self.node_layout.addWidget(self.thread_btn, 3, 1)

        # selected consensus is PoS, connect to Qtum
        elif self.BlockchainIoT.consensus == 'PoS':
            start_command = "~/CrossChain/PoA/run.sh"
            self.ssh_client.exec_command(start_command)
            self.BlockchainIoT.connected_nodes[self.name] = self
            self.worker1 = QTumWorker(self.stop_thread, self.ssh_client, self.pos_block_num)
            self.thread1 = QThread()
            self.worker1.moveToThread(self.thread1)  # move the worker object to the thread object
            self.worker1.block_signal.connect(self.filtering)  # connects worker's signal to accept block
            self.worker1.finished.connect(self.thread1.quit)  # connect worker's finish signal to thread
            self.thread1.started.connect(self.worker1.block_monitor)  # connect thread signal to worker operation method
            self.thread1.start()  # start the thread

        elif self.BlockchainIoT.consensus == 'PoA':
            start_command = "~/CrossChain/PoA/" + self.chain_name + "/run.sh"
            self.ssh_client.exec_command(start_command)
            time.sleep(10)
            self.ip_address = socket.gethostbyname(self.name.partition('@')[2])
            self.web3 = Web3(Web3.HTTPProvider('http://' + self.ip_address + ':' + self.port))
            self.BlockchainIoT.connected_nodes[self.name] = self
            self.BlockchainIoT.print_console.append("Node " + self.name + "connected to " + str(self.chain_name) + '\n')

            self.node_address = self.web3.eth.coinbase
            self.balance = self.web3.fromWei(self.web3.eth.getBalance(self.node_address), "ether")
            self.balance_label.setText("Balance: " + str(self.balance))

            self.port_label.hide()
            self.port_btn.hide()

        self.BlockchainIoT.ips.append(self.ip_address)

        self.remove.show()
        self.transact_amount.show()
        self.transact_location.show()
        self.transact_button.show()
        self.start_mining_btn.show()
        self.stop_mining_btn.show()
        self.stop_mining_btn.setEnabled(False)
        self.connect.hide()

    def set_port(self):
        self.port = str(self.port_entry.text())
        self.port_label.setText("Port: " + self.port)

    def set_pass(self):
        self.password = str(self.pass_entry.text())
        self.BlockchainIoT.print_console.append("Password Set \n")

    def set_thread(self):
        self.thread_num = int(self.thread_entry.text())
        self.BlockchainIoT.print_console.append("Thread Num set: " + str(self.thread_num) + "\n")

    def transact(self):
        amount = self.transact_amount.text()
        amount = self.web3.toWei(amount, "ether")
        location = self.transact_location.text()
        receiver = self.BlockchainIoT.connected_nodes[location]

        transaction = {
                        'from': self.web3.toChecksumAddress(self.node_address),
                        'to': self.web3.toChecksumAddress(receiver.node_address),
                        'value': amount
                      }


        self.web3.eth.sendTransaction(transaction)
        self.BlockchainIoT.print_console.append('Transaction Sent' + "\n")

    def start_mining(self):
        self.web3.miner.start(self.thread_num)
        self.stop_thread = False

        block_filter = self.web3.eth.filter('latest')

        try:
            # creates worker and thread objects
            self.worker1 = GethWorker(self.web3, block_filter, self.stop_thread, self.ssh_client)
            self.thread1 = QThread()
            self.worker1.moveToThread(self.thread1)  # move the worker object to the thread object
            self.worker1.block_signal.connect(self.filtering)  # connects worker's signal to accept block
            self.worker1.finished.connect(self.thread1.quit)  # connect worker's finish signal to thread
            self.thread1.started.connect(self.worker1.block_monitor)  # connect thread signal to worker operation method
            self.thread1.start()  # start the thread
        except:
            print('worker1 error')

        self.start_mining_btn.setEnabled(False)
        self.stop_mining_btn.setEnabled(True)

        self.BlockchainIoT.print_console.append(str(self.name) + " has started mining" + "\n")

    def stop_mining(self):
        self.web3.miner.stop()
        self.stop_thread = True

        self.start_mining_btn.setEnabled(True)
        self.stop_mining_btn.setEnabled(False)
        self.BlockchainIoT.print_console.append(str(self.name) + " has stopped mining" + "\n")

    # filter stores data everytime a block is mined
    def filtering(self, block):
        if self.BlockchainIoT.consensus == 'PoW':
            # Checks if this node mined the block
            if block['miner'] == self.node_address:
                self.latest_block = block

                new_time = self.latest_block['timestamp']
                time_between = new_time - self.BlockchainIoT.last_block_time
                self.latest_block['time since last block'] = time_between
                self.BlockchainIoT.last_block_time = new_time

                self.BlockchainIoT.log_block(self.name, self.latest_block)

                if self.BlockchainIoT.realtime is False:
                    self.BlockchainIoT.add_data(self)

            for node in self.BlockchainIoT.connected_nodes.values():
                node.balance = node.web3.eth.getBalance(node.node_address)
                node.balance = node.balance / (10 ** 18)
                node.balance_label.setText("Balance: " + str(node.balance))

        elif self.BlockchainIoT.consensus == 'PoS':
            self.latest_block = block

            if self.latest_block['number'] != self.pos_block_num:
                new_time = self.latest_block['timestamp']
                time_between = new_time - self.BlockchainIoT.last_block_time
                self.latest_block['time since last block'] = time_between
                self.BlockchainIoT.last_block_time = new_time

                self.pos_block_num = self.latest_block['number']
                self.BlockchainIoT.log_block(self.name, self.latest_block)

                if self.BlockchainIoT.realtime is False:
                    self.BlockchainIoT.add_data(self)

        elif self.BlockchainIoT.consensus == 'PoA':
            if block['miner'] == self.node_address:
                self.latest_block = block

                new_time = self.latest_block['timestamp']
                time_between = new_time - self.BlockchainIoT.last_block_time
                self.latest_block['time since last block'] = time_between
                self.BlockchainIoT.last_block_time = new_time

                self.BlockchainIoT.log_block(self.name, self.latest_block)

                if self.BlockchainIoT.realtime is False:
                    self.BlockchainIoT.add_data(self)

            for node in self.BlockchainIoT.connected_nodes.values():
                bal = int(node.web3.eth.getBalance(node.node_address))
                node.balance = bal/(10**18)
                node.balance_label.setText("Balance: " + str(node.balance))

    # gets system and network data via multiple threads periodically
    def sample_netsys(self):
        # creates worker and thread objects
        try:
            self.worker2 = CpuUsageWorker(self.ssh_client)
            self.thread2 = QThread()
            self.worker2.moveToThread(self.thread2)
            self.worker2.cpu_usage_signal.connect(self.store_netsys)
            self.worker2.finished.connect(self.thread2.quit)
            self.thread2.started.connect(self.worker2.cpu_usage_monitor)
            self.thread2.start()
        except:
            print('worker2 error')

        try:
            self.worker3 = NetDataRate(self.ssh_client)
            self.thread3 = QThread()
            self.worker3.moveToThread(self.thread3)
            self.worker3.net_data_signal.connect(self.store_netsys)
            self.worker3.finished.connect(self.thread3.quit)
            self.thread3.started.connect(self.worker3.net_data_monitor)
            self.thread3.start()
        except:
            print('worker3 error')

        try:
            self.worker4 = NetLatencyJitter(self.ssh_client, self.BlockchainIoT.ips, self.ip_address)
            self.thread4 = QThread()
            self.worker4.moveToThread(self.thread4)
            self.worker4.net_lj_signal.connect(self.store_netsys)
            self.worker4.finished.connect(self.thread4.quit)
            self.thread4.started.connect(self.worker4.net_lj_monitor)
            self.thread4.start()
        except:
            print('worker4 error')

    # stores information in a dictionary
    def store_netsys(self, dict_snippet):
        self.netsys_data.update(dict_snippet)

        if len(self.netsys_data) == 9:
            self.BlockchainIoT.log_netsys(self.netsys_data, self.name)
            if self.BlockchainIoT.realtime is True:
                self.BlockchainIoT.add_data(self)

    # disconnects from ssh
    def disconnect_ssh(self):
        self.ssh_client.exec_command("killall geth")
        self.ssh_client.exec_command("killall qtum")
        self.ssh_client.exec_command("killall parity")
        self.ssh_client.close()
        self.close()

        del self.BlockchainIoT.connected_nodes[self.name]
        self.BlockchainIoT.nodes = [x for x in self.BlockchainIoT.nodes if not (x.name == self.name and x.ssh_client ==
                                                                                self.ssh_client)]

        self.BlockchainIoT.print_console.append('Disconnected' + "\n")


class GethWorker(QObject):
    block_signal = pyqtSignal(object)
    finished = pyqtSignal()

    def __init__(self, web3, block_filter, stop_thread, ssh_client):
        QObject.__init__(self)
        self.web3 = web3
        self.block_filter = block_filter
        self.stop_thread = stop_thread
        self.ssh_client = ssh_client

    @pyqtSlot()
    def block_monitor(self):
        while True:
            for event in self.block_filter.get_new_entries():
                # grabs block data, puts into a dictionary, and emits signal
                block = self.web3.eth.getBlock('latest')
                miner = block.miner
                timestamp = block.timestamp
                number = block.number
                difficulty = block.difficulty
                transactions = len(block.transactions)
                size = block.size
                block_dict = {'timestamp': timestamp, 'miner': miner, 'number': number, 'difficulty': difficulty,
                              'number_transactions': transactions, 'size': size}

                self.block_signal.emit(block_dict)

            if self.stop_thread:
                self.BlockchainIoT.print_console.append('worker1 stop' + "\n")
                self.finished.emit()
                break


# QTum does not have an API/event signal that fires when a new block is generated so instead a new block is continuously
# checked for
class QTumWorker(QObject):
    block_signal = pyqtSignal(object)
    finished = pyqtSignal()

    def __init__(self, stop_thread, ssh_client, block_num):
        QObject.__init__(self)
        self.stop_thread = stop_thread
        self.ssh_client = ssh_client
        self.block_num = block_num

    @pyqtSlot()
    def block_monitor(self):
        while True:
            stdin, stdout, stderr = self.ssh_client.exec_command("qtum-cli -regtest getmininginfo")
            mining_info = stdout.readlines()
            number = int(re.findall(r'\d+', mining_info[1])[0])

            if number != self.block_num:
                difficulty_dict = re.findall(r'\d+', mining_info[6])
                difficulty = float(difficulty_dict[0] + "." + difficulty_dict[1] + "e-" + difficulty_dict[2])
                command1 = 'qtum-cli -regtest getblockhash ' + str(number)
                stdin, stdout, stderr = self.ssh_client.exec_command(command1)
                block_hash = stdout.readlines()[0]
                command2 = 'qtum-cli -regtest getblock ' + str(block_hash)
                stdin, stdout, stderr = self.ssh_client.exec_command(command2)
                block_info = stdout.readlines()
                size = float(re.findall(r'\d+', block_info[4])[0])
                timestamp = float(re.findall(r'\d+', block_info[16])[0])
                number_transactions = float(re.findall(r'\d+', block_info[22])[0])

                block_dict = {'timestamp': timestamp, 'number': number, 'difficulty': difficulty, 'size': size,
                              'number transactions': number_transactions}
                self.block_signal.emit(block_dict)

            elif self.stop_thread:
                self.finished.emit()
                break


class CpuUsageWorker(QObject):
    cpu_usage_signal = pyqtSignal(object)
    finished = pyqtSignal()

    def __init__(self, ssh_client):
        QObject.__init__(self)
        self.ssh_client = ssh_client

    @pyqtSlot()
    def cpu_usage_monitor(self):
        stdin, stdout, stderr = self.ssh_client.exec_command(
            """cat <(grep 'cpu ' /proc/stat) <(sleep 1 && grep 'cpu ' /proc/stat) | awk -v RS="" '
                {print ((($16 + $17) + ($13 + $14 + $15 + $18 + $19 + $20) - ($5 + $6) - ($2 + $3 + $4 + $7 + $8 + $9)) - (($16 + $17) - ($5 + $6))) / (($16 + $17) + ($13 + $14 + $15 + $18 + $19 + $20) - ($5 + $6) - ($2 + $3 + $4 + $7 + $8 + $9)) * 100}'""")
        cpu_usage = stdout.readlines()[0]
        cpu_usage = cpu_usage.strip('\n')

        diction = {'cpu usage': float(cpu_usage)}
        self.cpu_usage_signal.emit(diction)
        self.finished.emit()


class NetDataRate(QObject):
    net_data_signal = pyqtSignal(object)
    finished = pyqtSignal()

    def __init__(self, ssh_client):
        QObject.__init__(self)
        self.ssh_client = ssh_client

    @pyqtSlot()
    def net_data_monitor(self):
        stdin, stdout, stderr = self.ssh_client.exec_command("bwm-ng -o csv -c 1")
        net = stdout.readlines()
        for line in net:
            if "eth0" in line:
                network = line
        network_info = network.split(';')

        network_data = {}

        try:
            network_data['timestamp'] = float(network_info[0])
            network_data['network name'] = network_info[1]
            network_data['bytes out/s'] = float(network_info[2])
            network_data['bytes in/s'] = float(network_info[3])
            network_data['packets out/s'] = float(network_info[7])
            network_data['packets in/s'] = float(network_info[8])

        except IndexError:
            print('no data rate data was received')
            network_data['timestamp'] = None
            network_data['network name'] = None
            network_data['bytes out/s'] = None
            network_data['bytes in/s'] = None
            network_data['packets out/s'] = None
            network_data['packets in/s'] = None

        self.net_data_signal.emit(network_data)
        self.finished.emit()


class NetLatencyJitter(QObject):
    net_lj_signal = pyqtSignal(object)
    finished = pyqtSignal()

    def __init__(self, ssh_client, ips, self_ip):
        QObject.__init__(self)
        self.ssh_client = ssh_client
        self.ips = ips
        self.self_ip = self_ip
        self.latency = []
        self.jitter = []

    def average(self, lst):
        return sum(lst) / len(lst)

    @pyqtSlot()
    def net_lj_monitor(self):
        network_data = {}
        for address in self.ips:
            if str(self.self_ip) is not str(address):
                command = "ping -c 3 -i 0.2 " + str(address)
                stdin, stdout, stderr = self.ssh_client.exec_command(command)
                network_info2 = stdout.readlines()

                def jitter(latency_array):
                    past = None
                    diff_arr = []
                    for lat in latency_array:
                        if past is not None:
                            diff = abs(lat - past)
                            diff_arr.append(diff)
                        past = lat
                    return sum(diff_arr) / len(diff_arr)

                lc = 0
                request_count = 3
                latency_arr = [None] * request_count

                for line in network_info2:
                    if 1 <= lc <= request_count:
                        word_list = line.split(" ")
                        latency = float(word_list[6].lstrip("time="))
                        latency_arr[lc - 1] = latency
                    lc += 1

                self.latency.append(float(sum(latency_arr) / len(latency_arr)))
                self.jitter.append(float(jitter(latency_arr)))

                network_data = {'Avg Latency': self.average(self.latency), 'Avg Jitter': self.average(self.jitter)}

        self.net_lj_signal.emit(network_data)
        self.finished.emit()


class PastGraph(QWidget): 
    def __init__(self, log_loc):
        QWidget.__init__(self)
        self.log_loc = log_loc
        self.plot = None
        self.legend = None
        self.block_data_param = ['Number', 'Difficulty', 'Number of Transactions', 'Size of Block',
                                 'Time Since Last Block']
        self.graph_x = {}
        self.graph_y = {}

        self.last_block_x = []
        self.last_block_y = []

        self.attribute_drop_down = QComboBox()
        self.attribute_drop_down.addItems(['Number', 'Difficulty', 'Number of Transactions', 'Size of Block',
                                           'Time Since Last Block', 'CPU Usage', 'Network Bandwidth Out',
                                           'Network Bandwidth In', 'Network Packet Out', 'Network Packet In', 'Avg Latency',
                                           'Jitter'])

        self.consensus_drop_down = QComboBox()
        self.consensus_drop_down.addItems(['PoW', 'PoS', 'PoA'])

        self.graph_btn = QPushButton("Graph")
        self.graph_btn.clicked.connect(self.graph)

        self.layout = QGridLayout()
        self.layout.addWidget(self.attribute_drop_down, 0, 0)
        self.layout.addWidget(self.consensus_drop_down, 1, 0)
        self.layout.addWidget(self.graph_btn, 2, 0)

        self.setLayout(self.layout)

        self.error_label = QLabel('Data Not Found')

    def graph(self):
        self.attribute_drop_down.hide()
        self.consensus_drop_down.hide()
        self.graph_btn.hide()

        try:
            axis = DateAxis(orientation='bottom')
            pg.setConfigOption('background', 'w')
            self.plot = pg.PlotWidget(axisItems={'bottom': axis})
            self.plot.setMinimumWidth(1100)
            self.plot.setMinimumHeight(600)
            self.legend = self.plot.addLegend()
            self.legend.setMaximumWidth(175)
            self.plot.setLabel('bottom', 'Time', None)
            self.layout.addWidget(self.plot, 0, 0)

            if str(self.attribute_drop_down.currentText()) in self.block_data_param:
                for file in os.listdir(self.log_loc):
                    filename = os.fsdecode(file)
                    if "Blocks" in filename and str(self.consensus_drop_down.currentText()) in filename:
                        name = filename[:-15]
                        x_data = []
                        y_data = []
                        data = csv.DictReader(open(self.log_loc + filename))
                        for row in data:
                            if str(self.attribute_drop_down.currentText()) == 'Time Since Last Block':
                                self.last_block_x.append(float(row['Number']))
                                self.last_block_y.append(float(row['Time Since Last Block']))
                            else:
                                x_data.append(float(row['Timestamp']))
                                y_data.append(float(row[self.attribute_drop_down.currentText()]))

                        self.graph_x[name] = x_data
                        self.graph_y[name] = y_data

            if str(self.attribute_drop_down.currentText()) not in self.block_data_param:
                for file in os.listdir(self.log_loc):
                    filename = os.fsdecode(file)
                    if "NetSys" in filename and str(self.consensus_drop_down.currentText()) in filename:
                        name = filename[:-15]
                        x_data = []
                        y_data = []
                        data = csv.DictReader(open(self.log_loc + filename))
                        for row in data:
                            x_data.append(float(row['Timestamp']))
                            y_data.append(float(row[self.attribute_drop_down.currentText()]))

                        self.graph_x[name] = x_data
                        self.graph_y[name] = y_data

            color_number = int(0)
            if str(self.attribute_drop_down.currentText()) == 'CPU Usage':
                self.plot.setLabel('left', 'CPU Usage', '%')
            elif str(self.attribute_drop_down.currentText()) == 'Network Bandwidth Out':
                self.plot.setLabel('left', 'Network Bandwidth Out', 'Bytes/s')
            elif str(self.attribute_drop_down.currentText()) == 'Network Bandwidth In':
                self.plot.setLabel('left', 'Network Bandwidth In', 'Bytes/s')
            elif str(self.attribute_drop_down.currentText()) == 'Network Packet Out':
                self.plot.setLabel('left', 'Network Packets Out', 'Packets/s')
            elif str(self.attribute_drop_down.currentText()) == 'Network Packet In':
                self.plot.setLabel('left', 'Network Packets In', 'Packets/s')
            elif str(self.attribute_drop_down.currentText()) == 'Jitter':
                self.plot.setLabel('left', 'Avg Jitter', 's')
            elif str(self.attribute_drop_down.currentText()) == 'Avg Latency':
                self.plot.setLabel('left', 'Avg Latency', 's')
            elif str(self.attribute_drop_down.currentText()) == 'Difficulty':
                self.plot.setLabel('left', 'Difficulty', '')
            elif str(self.attribute_drop_down.currentText()) == 'Number of Transactions':
                self.plot.setLabel('left', '# Transactions per Block', '')
            elif str(self.attribute_drop_down.currentText()) == 'Size of Block':
                self.plot.setLabel('left', 'Size', 'Bytes')
            elif str(self.attribute_drop_down.currentText()) == 'Number':
                self.plot.setLabel('left', 'Number of Blocks', '')

            if str(self.attribute_drop_down.currentText()) == 'Time Since Last Block':
                self.plot = pg.PlotWidget()
                self.plot.setMinimumWidth(1100)
                self.plot.setMinimumHeight(600)
                self.layout.addWidget(self.plot, 0, 0)
                self.plot.setLabel('bottom', 'Block Number', '')
                self.plot.setLabel('left', 'Time From Last Block', 's')
                self.plot.plot(self.last_block_x[1:], self.last_block_y[1:], pen='b', symbolBrush='b')
            else:
                for name in self.graph_x:
                    x_value = self.graph_x[name]
                    y_value = self.graph_y[name]

                    plot_item = self.plot.plot(x_value, y_value, pen=pg.intColor(color_number),
                                               symbolBrush=pg.intColor(color_number))
                    self.legend.addItem(plot_item, name)
                    color_number += 1

        except Exception as e:
            self.attribute_drop_down.show()
            self.consensus_drop_down.show()
            self.graph_btn.show()
            self.layout.addWidget(self.error_label, 3, 0)
            print(e)


# creates an axisItem that allows time values to be represented nicely. From pyqtgraph documentation examples
class DateAxis(pg.AxisItem):
    def tickStrings(self, values, scale, spacing):
        strns = []
        rng = 0
        try:
            rng = max(values)-min(values)
        except ValueError:
            print("Empty values")
        if rng < 3600*24:
            string = '%H:%M:%S'
            label1 = '%b %d -'
            label2 = ' %b %d, %Y'
        elif rng >= 3600*24 and rng < 3600*24*30:
            string = '%d'
            label1 = '%b - '
            label2 = '%b, %Y'
        elif rng >= 3600*24*30 and rng < 3600*24*30*24:
            string = '%b'
            label1 = '%Y -'
            label2 = ' %Y'
        elif rng >=3600*24*30*24:
            string = '%Y'
            label1 = ''
            label2 = ''
        for x in values:
            try:
                strns.append(time.strftime(string, time.localtime(x)))
            except ValueError:  # Windows can't handle dates before 1970
                strns.append('')
            except OSError:
                print("Invalid Argument")
            except OverflowError:
                print("Overflow time_t")
        return strns


class SampleWarning(QWidget):
    def __init__(self, parent):
        QWidget.__init__(self)
        self.parent = parent
        self.layout = QGridLayout()
        self.message = QLabel('WARNING: Setting a sample rate below around 2500 may cause a QThread Error')
        self.layout.addWidget(self.message, 0, 0, 1, 2)

        self.exit = QPushButton("Cancel")
        self.exit.clicked.connect(self.delete)

        self.proceed = QPushButton("Proceed")
        self.proceed.clicked.connect(self.cont)

        self.layout.addWidget(self.exit, 1, 0, 1, 1)
        self.layout.addWidget(self.proceed, 1, 1, 1, 1)
        self.setLayout(self.layout)

    def cont(self):
        self.parent.set_sample(True)
        self.close()

    def delete(self):
        self.close()


class FileError(QWidget):
    def __init__(self, file):
        QWidget.__init__(self)
        self.layout = QGridLayout()
        self.message = QLabel('ERROR: No repo/file found here: ' + file +
                              "\n Please create the file or set a location elsewhere")
        self.layout.addWidget(self.message, 0, 0, 1, 2)

        self.setLayout(self.layout)


class SwitchHelper(QWidget):
    def __init__(self, parent):
        self.parent = parent
        QWidget.__init__(self)
        self.layout = QGridLayout()

        self.chain_name_label = QLineEdit()
        self.chain_name_label.setPlaceholderText('Set new Chain Name (required)')
        self.layout.addWidget(self.chain_name_label, 0, 0)

        self.port_entry = QLineEdit()
        self.port_entry.setPlaceholderText('Set new RPC Port (optional)')
        self.layout.addWidget(self.port_entry, 1, 0)

        self.proceed = QPushButton("Proceed")
        self.proceed.clicked.connect(self.switch)
        self.layout.addWidget(self.proceed, 2, 0)

        self.setLayout(self.layout)

    def switch(self):
        name = self.chain_name_label.text()
        port = self.port_entry.text()

        self.parent.switch(name, port)
        self.close()


class DBConnection:
    def __init__(self):
        self.conn = sqlite3.connect('credentials.db')
        self.cursor = self.conn.cursor()
        self.cursor.execute('''CREATE table IF NOT EXISTS infos (username TEXT NOT NULL, hostname TEXT NOT NULL, password TEXT NOT NULL, UNIQUE(username, hostname))''')
        self.cursor.execute('SELECT * FROM infos')


if __name__ == '__main__':
    app = QApplication(sys.argv)
    gallery = BlockchainIoT()
    gallery.show()
    sys.exit(app.exec_())
