# Measurement of Overhead
## Experimental set up
The system is set that Node2 is connected to Node1, Node6 is connected to Node1.  
## Hardware stats
### cpu usage  
   ```time cat <(grep 'cpu ' /proc/stat) <(sleep 1 && grep 'cpu ' /proc/stat) | awk -v RS="" ' {print ($13-$2+$15-$4)*100/($13-$2+$15-$4+$16-$5)}' ```
   We do it 3 times on each node of 1, 2, 6, and calculate the average.  
   1.028
   1.027
   1.029
   1.029
   1.023
   1.022
   1.031
   1.027
   1.029
   Average: 1.0272
### Network bandwidth
   ```time bwm-ng -o csv -c 1```
   We do it 3 times on each node of 1, 2, 6, and calculate the average.  
   0.511
   0.507
   0.507
   0.511
   0.510
   0.511
   0.520
   0.511
   0.511
   Average: 0.511
### Jitter and latency
   ```time ping -c 5 -i 0.2 $IP_ADDRESS_OF_ANOTHER_NODE``` We test Node6 to Node1, Node2 to Node1, Node1 to Node6  
   6->1
   0.828
   0.827
   0.827
   2->1
   0.832
   0.823
   0.823
   1->6
   0.820
   0.823
   0.820

   Average: 0.8248

## RPC calls
### PoS
```time qtum-cli -regtest getmininginfo```
52.364s
44.987s
28.598s
0.016s
0.018s
0.018s
0.016s
0.017s
0.018s
Average: 14.008
### PoW and PoA
```
from web3 import Web3
import time
start = time.time()
web3 = Web3(Web3.HTTPProvider('http://' + '172.16.9.183' + ':8546'))
block = web3.eth.getBlock('latest')
print(block.difficulty)
end = time.time()
print(end - start)
```
PoW
0.968
0.0267
0.026
0.896
0.021
0.045
1.073
0.312
0.036
Average: 0.37

PoA
0.04681849479675293
0.013655424118041992
0.013631820678710938
0.04326057434082031
0.013579607009887695
0.013871192932128906
0.04673266410827637
0.015143871307373047
0.014321088790893555
Average 0.024