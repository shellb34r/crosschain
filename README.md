# Crosschain

Ongoing.md: Goals and tasks that we hope to accomplish  
Progress_Report.md: What we've tried so far   
Documentation: Documents how to recreate successful steps  
- PoW: How to setup PoW Ethereum on Ubuntu RPi
- adhoc: How to setup wireless adhoc network among RPis
- docker: How to install docker on Pi
- privatenet: How to setup a private blockchain network on RPi

``` bash
~
│   .qtum
│
└───CrossChain
   │      
   └───PoW
   |    └───Chain1
   |    |   └─── ...  
   |    └───Chain2
   |    └───...
   | 
   └───PoS
       │   run.sh
   |  
   └───PoW
   |    └───Chain1
   |    |   └─── ...  
   |    └───Chain2
   |    └───...    
```